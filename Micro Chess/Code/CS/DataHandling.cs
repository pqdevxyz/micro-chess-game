﻿using System;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace WpfMicroChess2_GD
{
    class DataHandling
    {
        private StreamWriter sw;
        private StreamReader sr;
        private SqlConnection sqlConnect;
        private SqlCommand sqlCommand;
        private SqlDataReader sqlDataReader;

        private void sqlInit()
        {
            sqlConnect = new SqlConnection();
            sqlConnect.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=H:\WpfMicroChess1\WpfMicroChess1\Database.mdf;Integrated Security=True";
        }
        public void writeGameDataToFile(Board board, string filePath, string fileName, string password, bool addToEnd)
        {
            sw = new StreamWriter(filePath + fileName + ".csg", addToEnd);
            sw.WriteLine(password);

            for (int i = 0; i < 10; i++)
            {
                sw.WriteLine(board.bitboard[i]);
            }
            sw.WriteLine(board.numberOfMove);
            sw.WriteLine(board.WKMoved);
            sw.WriteLine(board.WRMoved);
            sw.WriteLine(board.BKMoved);
            sw.WriteLine(board.BRMoved);
            sw.WriteLine(board.WKInCheck);
            sw.WriteLine(board.BKInCheck);
            sw.WriteLine(board.GameOn);
            sw.WriteLine(board.lastMoveEndSquare);
            sw.WriteLine((int)board.lastMovePiece);
            sw.WriteLine(board.lastMoveStartSquare);
            sw.WriteLine(board.WCastleMade);
            sw.WriteLine(board.BCastleMade);
            sw.WriteLine(AppData.aiDiff);
            sw.WriteLine(AppData.playerIsWhite);
            sw.WriteLine(AppData.aiGame);
            sw.Close();
        }
        public void writeGameDataToFile(Board board, string filePath, string fileName, string password)
        {
            sw = new StreamWriter(filePath + fileName + ".csg");
            sw.WriteLine(password);

            for (int i = 0; i < 10; i++)
            {
                sw.WriteLine(board.bitboard[i]);
            }
            sw.WriteLine(board.numberOfMove);
            sw.WriteLine(board.WKMoved);
            sw.WriteLine(board.WRMoved);
            sw.WriteLine(board.BKMoved);
            sw.WriteLine(board.BRMoved);
            sw.WriteLine(board.WKInCheck);
            sw.WriteLine(board.BKInCheck);
            sw.WriteLine(board.GameOn);
            sw.WriteLine(board.lastMoveEndSquare);
            sw.WriteLine((int)board.lastMovePiece);
            sw.WriteLine(board.lastMoveStartSquare);
            sw.WriteLine(board.WCastleMade);
            sw.WriteLine(board.BCastleMade);
            sw.WriteLine(AppData.aiDiff);
            sw.WriteLine(AppData.playerIsWhite);
            sw.WriteLine(AppData.aiGame);
            sw.Close();
        }
        public bool readGameDataFromFile(ref Board board, string filePath, string fileName, string password)
        {
            sr = new StreamReader(filePath + fileName + ".csg");
            if (password == sr.ReadLine())
            {
                for (int i = 0; i < 10; i++)
                {
                    board.bitboard[i] = Convert.ToInt32(sr.ReadLine());
                }
                board.numberOfMove = Convert.ToInt32(sr.ReadLine());
                board.WKMoved = Convert.ToBoolean(sr.ReadLine());
                board.WRMoved = Convert.ToBoolean(sr.ReadLine());
                board.BKMoved = Convert.ToBoolean(sr.ReadLine());
                board.BRMoved = Convert.ToBoolean(sr.ReadLine());
                board.WKInCheck = Convert.ToBoolean(sr.ReadLine());
                board.BKInCheck = Convert.ToBoolean(sr.ReadLine());
                board.GameOn = Convert.ToBoolean(sr.ReadLine());
                board.lastMoveEndSquare = Convert.ToInt32(sr.ReadLine());
                board.lastMovePiece = (typesOfPiece)Convert.ToInt32(sr.ReadLine());
                board.lastMoveStartSquare = Convert.ToInt32(sr.ReadLine());
                board.WCastleMade = Convert.ToBoolean(sr.ReadLine());
                board.BCastleMade = Convert.ToBoolean(sr.ReadLine());
                AppData.aiDiff = Convert.ToInt32(sr.ReadLine());
                AppData.playerIsWhite = Convert.ToBoolean(sr.ReadLine());
                AppData.aiGame = Convert.ToBoolean(sr.ReadLine());
                sr.Close();
                return true;
            }
            else
                return false;
        }
        public int writeToSql(Board board, int startSquare, int endSquare, typesOfPiece type)
        {
            sqlInit();
            using (sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = sqlConnect;//sets the connection to that of the database
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "IF NOT EXISTS (SELECT * FROM Moves WHERE HashValue = @HashValue) INSERT INTO Moves (HashValue,StartSquare,EndSquare,PieceToMove) VALUES (@HashValue,@StartSquare,@EndSquare,@PieceToMove)";//sets the command to add to the database
                sqlCommand.Parameters.AddWithValue("@HashValue", board.Hvalue);//passes in data saftly avoiding SQLi
                sqlCommand.Parameters.AddWithValue("@StartSquare", startSquare);
                sqlCommand.Parameters.AddWithValue("@EndSquare", endSquare);
                sqlCommand.Parameters.AddWithValue("@PieceToMove", (int)type);
                try
                {
                    sqlConnect.Open();//opens connection
                    return sqlCommand.ExecuteNonQuery();//runs command
                }
                finally
                {
                    sqlConnect.Close();//closes connection
                }
            }
        }//TODO add checkpiece and checksquare
        public void readFromSql()
        {
            sqlInit();
            try
            {
                sqlConnect.Open();//opens connection
                sqlDataReader = null;
                sqlCommand = new SqlCommand("SELECT * FROM Moves", sqlConnect);
                sqlDataReader = sqlCommand.ExecuteReader();//runs command
                while (sqlDataReader.Read())
                {
                    Console.WriteLine("H:{0} | SS:{1} | ES:{2} | P:{3} | CP:{4} | CS:{5}", sqlDataReader["HashValue"].ToString(), sqlDataReader["StartSquare"].ToString(), sqlDataReader["EndSquare"].ToString(), sqlDataReader["PieceToMove"].ToString(), sqlDataReader["CheckPieceSqaure"].ToString(), sqlDataReader["CheckPieceType"].ToString());
                }

            }
            finally
            {
                sqlConnect.Close();//closes connection
            }
        }
    }
}
