﻿using System;

namespace WpfMicroChess2_GD
{
    public enum typesOfPiece { WK, WN, WB, WR, WP, BK, BN, BB, BR, BP };
    public class Board
    {
        private Debug d = new Debug("Board");//creates object used to write to output window in vs

        public const int totalSquareNum = 20;//number of squares on the board
        public const int totalDiffPiece = 5;//number of different types of piece in the game e.g. K,N,B,R,P
        public const int modNumber = 1000000;//the number of rows in the database used for hashing
        public const typesOfPiece promotionTypeWhite = typesOfPiece.WN;//set to the piece that the white pawn should be promoted to
        public const typesOfPiece promotionTypeBlack = (promotionTypeWhite + totalDiffPiece);//auto set from the piece you set a the white promotion                                                            

        private int evalu = 0;//a value of the current evaluated game +ve in Black -ve in White - chance of win
        private int hvalue;//the hash value of the current board
        private bool wkInCheck = false, bkInCheck = false;
        private bool wkMoved = false, wrMoved = false, bkMoved = false, brMoved = false,wCastleMade = false,bCastleMade = false;//used to see a castle move is legal
        private bool gameOn = true;

        private readonly int[] pieceValue = new int[totalDiffPiece * 2] { 120, 50, 30, 30, 10, -120, -50, -30, -30, -10 };//the vaule given to all pieces.
        private readonly int[] boardValueNumbers = new int[totalSquareNum] { 405, 847, 19, 684, 661, 921, 29, 502, 407, 494, 331, 658, 970, 481, 433, 72, 62, 755, 743, 78 };//random numbers assigned to each position on the board for hashing

        public int[] bitboard = new int[totalDiffPiece * 2];

        public int Hvalue
        {
            get
            {
                hashValue();
                return hvalue;
            }
        }
        public int Evalue
        {
            get
            {
                evaluateBoard();
                return evalu;
            }
        }
        public bool WKMoved
        {
            get
            {
                return wkMoved;
            }
            set
            {
                wkMoved = value;
            }
        }
        public bool WRMoved
        {
            get
            {
                return wrMoved;
            }
            set
            {
                wrMoved = value;
            }
        }
        public bool BKMoved
        {
            get
            {
                return bkMoved;
            }
            set
            {
                bkMoved = value;
            }
        }
        public bool BRMoved
        {
            get
            {
                return brMoved;
            }
            set
            {
                brMoved = value;
            }
        }
        public bool WKInCheck
        {
            get
            {
                return wkInCheck;
            }
            set
            {
                wkInCheck = value;
            }
        }
        public bool BKInCheck
        {
            get
            {
                return bkInCheck;
            }
            set
            {
                bkInCheck = value;
            }
        }
        public bool GameOn
        {
            get
            {
                return gameOn;
            }
            set
            {
                gameOn = value;
            }
        }
        public bool WCastleMade
        {
            get
            {
                return wCastleMade;
            }
            set
            {
                wCastleMade = value;
            }
        }
        public bool BCastleMade
        {
            get
            {
                return bCastleMade;
            }
            set
            {
                bCastleMade = value;
            }
        }

        public int numberOfMove = 0;
        public bool playerIsWhite = true;
        public bool singlePlayerGame = true;
        public typesOfPiece lastMovePiece;
        public int lastMoveStartSquare, lastMoveEndSquare;
        public bool lastMoveWasAi = false;

        public delegate void GameOnEventHandler(object sender, EventArgs e);
        public event GameOnEventHandler GameOnChanged;

        protected virtual void OnGameOnChanged()
        {
            if (GameOnChanged != null)
            {
                GameOnChanged(this, EventArgs.Empty);
            }
        }

        public Board()
        {
            d.outputToWindow("% - New Board Constructed - %");//write to output window in vs

            bitboard[(int)typesOfPiece.WK] = 1 << 0;
            bitboard[(int)typesOfPiece.WN] = 1 << 1;
            bitboard[(int)typesOfPiece.WB] = 1 << 2;
            bitboard[(int)typesOfPiece.WR] = 1 << 3;
            bitboard[(int)typesOfPiece.WP] = 1 << 4;
            bitboard[(int)typesOfPiece.BK] = 1 << 19;
            bitboard[(int)typesOfPiece.BN] = 1 << 18;
            bitboard[(int)typesOfPiece.BB] = 1 << 17;
            bitboard[(int)typesOfPiece.BR] = 1 << 16;
            bitboard[(int)typesOfPiece.BP] = 1 << 15;

            hashValue();
        }
        public void clearBoard()
        {
            int a = 0;//counter
            d.outputToWindow("clearBoard");//write to output window in vs

            foreach (int element in bitboard)//sets all boards to 0
            {
                bitboard[a] = 0;
                a++;
            }
        }
        public void addPiece(int squareNum, typesOfPiece type)
        {
            int a = 1;//used to add
            d.outputToWindow("addPiece");//write to output window in vs

            a <<= squareNum;
            bitboard[(int)type] = (bitboard[(int)type] | a);//does or op on the board and the square to be added
        }
        public void removePiece(int squareNum, typesOfPiece type)
        {
            int a = 1, piece = (int)type;
            d.outputToWindow("removePiece");//write to output window in vs
            a <<= squareNum;
            a = ~a;
            bitboard[piece] &= a;
        }
        public int evaluateBoard()
        {
            int numberOfPieces, eval = 0;
            d.outputToWindow("evaluateBoard");//write to output window in vs

            for (int i = 0; i < (totalDiffPiece * 2); i++)
            {
                numberOfPieces = countPieces((typesOfPiece)i);
                eval = eval + (pieceValue[i] * numberOfPieces);//times the number of pieces by their weight and adds then to the current eval value
            }
            eval += evaluatePawn();
            eval += evaluateKing();
            eval += evaluateKnight();
            eval += evaluateBishop();
            eval += evaluateRook();
            evalu = eval;
            return eval;
        }
        public int countPieces(typesOfPiece type)
        {
            int a = 1, numOfPiecesFound = 0;//used for finding piece
            d.outputToWindow("countPieces");//write to output window in vs

            for (int i = 0; i < totalSquareNum; i++)
            {
                if ((bitboard[(int)type] & a) != 0)
                {
                    numOfPiecesFound++;
                }
                a <<= 1;
            }
            return numOfPiecesFound;

        }
        public bool isEmpty(int squareNumIn)
        {
            int a = 1, b = 0;
            //d.outputToWindow("isEmpty");//write to output window in vs

            a <<= squareNumIn;
            foreach (int element in bitboard)//check if a piece is in the square
            {
                if ((bitboard[b] & a) > 0)
                {
                    return false;//check if a piece is not in the square
                }
                else
                {
                    b++;
                }
            }
            return true;
        }
        public bool pieceOn(int squareNum, typesOfPiece type)
        {
            int a = 1;
            //d.outputToWindow("pieceOn");//write to output window in vs

            a <<= squareNum;
            if ((bitboard[(int)type] & a) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool checkMoveIsLegal(int startSquare, int endSquare, typesOfPiece type)
        {
            bool moveLegal = false;
            d.outputToWindow("checkMoveIsLegal");//write to output window in vs
            if (startSquare < 0 || endSquare > 20)
            {
                d.outputError(1, "move out of range of board <0 or >20");
                return false;//out of range
            }
            else
            {
                if ((int)type < totalDiffPiece)
                {
                    if (!checkIfKingIsInCheck(typesOfPiece.WK))
                    {
                        bool whitePieceCheck = (isWhite(endSquare) || isKing(endSquare));
                        if (!whitePieceCheck)//check is endspace is free or black
                        {
                            if (numberOfMove % 2 == 0)
                            {
                                switch (type)
                                {
                                    case (typesOfPiece.WK):
                                        {
                                            moveLegal = checkKingMoveIsLegal(startSquare, endSquare, typesOfPiece.WK);
                                            break;
                                        }
                                    case (typesOfPiece.WN):
                                        {
                                            moveLegal = checkKnightMoveIsLegal(startSquare, endSquare);
                                            break;
                                        }
                                    case (typesOfPiece.WB):
                                        {
                                            moveLegal = checkBishopMoveIsLegal(startSquare, endSquare, typesOfPiece.WB);
                                            break;
                                        }
                                    case (typesOfPiece.WR):
                                        {
                                            moveLegal = checkRookMoveIsLegal(startSquare, endSquare, typesOfPiece.WR, -1);
                                            break;
                                        }
                                    case (typesOfPiece.WP):
                                        {
                                            moveLegal = checkPawnMoveIsLegal(startSquare, endSquare, typesOfPiece.WP);
                                            break;
                                        }
                                }

                            }
                            else
                            {
                                d.outputError(4, "cannot move other players piece");
                                return false;
                            }
                        }
                        else
                        {
                            d.outputError(2, "cannot move to a space with piece of same colour on");
                            return false;
                        }
                    }
                    else
                    {
                        //king is in check
                        //check for mate
                    }
                }
                else
                {
                    bool blackPieceCheck = (isBlack(endSquare) || isKing(endSquare));
                    if (!blackPieceCheck)//check is endspace is free or white
                    {
                        if (numberOfMove % 2 == 1)
                        {
                            switch (type)
                            {
                                case (typesOfPiece.BK):
                                    {
                                        moveLegal = checkKingMoveIsLegal(startSquare, endSquare, typesOfPiece.BK);
                                        break;
                                    }
                                case (typesOfPiece.BN):
                                    {
                                        moveLegal = checkKnightMoveIsLegal(startSquare, endSquare);
                                        break;
                                    }
                                case (typesOfPiece.BB):
                                    {
                                        moveLegal = checkBishopMoveIsLegal(startSquare, endSquare, typesOfPiece.BB);
                                        break;
                                    }
                                case (typesOfPiece.BR):
                                    {
                                        moveLegal = checkRookMoveIsLegal(startSquare, endSquare, typesOfPiece.BR, -1);
                                        break;
                                    }
                                case (typesOfPiece.BP):
                                    {
                                        moveLegal = checkPawnMoveIsLegal(startSquare, endSquare, typesOfPiece.BP);
                                        break;
                                    }
                            }

                        }
                        else
                        {
                            d.outputError(4, "cannot move other players piece");
                            return false;
                        }
                    }
                    else
                    {
                        d.outputError(2, "cannot move to a space with piece of same colour on");
                        return false;
                    }
                }

                }
                return moveLegal;
        }
        public bool isWhite(int squareNum)
        {
            int a = 1;
            //d.outputToWindow("isWhite");//write to output window in vs

            a <<= squareNum;
            for (int b = 0; b < totalDiffPiece; b++)
            {
                if ((bitboard[b] & a) > 0)
                {
                    return true;
                }
            }
            return false;
        }
        public bool isBlack(int squareNum)
        {
            int a = 1;
            //d.outputToWindow("isBlack");//write to output window in vs

            a <<= squareNum;
            for (int b = 5; b < totalDiffPiece * 2; b++)
            {
                if ((bitboard[b] & a) > 0)
                {
                    return true;
                }
            }
            return false;
        }
        public void copyBoard(ref Board NewBoard)
        {
            d.outputToWindow("copyBoard");//write to output window in vs
            for (int i = 0; i < (totalDiffPiece * 2); i++)
            {
                NewBoard.bitboard[i] = bitboard[i];
            }
            NewBoard.numberOfMove = numberOfMove;
            NewBoard.wkMoved = wkMoved;
            NewBoard.wrMoved = wrMoved;
            NewBoard.bkMoved = bkMoved;
            NewBoard.brMoved = brMoved;
            NewBoard.wkInCheck = wkInCheck;
            NewBoard.bkInCheck = bkInCheck;
            NewBoard.gameOn = gameOn;
            NewBoard.evalu = evalu;
            NewBoard.hvalue = hvalue;
            NewBoard.bCastleMade = bCastleMade;
            NewBoard.wCastleMade = wCastleMade;
            NewBoard.gameOn = gameOn;
            NewBoard.lastMoveEndSquare = lastMoveEndSquare;
            NewBoard.lastMovePiece = lastMovePiece;
            NewBoard.lastMoveStartSquare = lastMoveStartSquare;
            NewBoard.lastMoveWasAi = lastMoveWasAi;
            NewBoard.playerIsWhite = playerIsWhite;
            NewBoard.singlePlayerGame = singlePlayerGame;
        }
        public int hashValue()
        {
            int i = 0, a = 0;
            d.outputToWindow("hashValue");//write to output window in vs

            //Empty x1,Black x2,White x3
            foreach (int element in bitboard)
            {
                for (i = 0; i < 20; i++)
                {
                    if (isEmpty(i) == true)
                    {
                        a = a + (boardValueNumbers[i]);
                    }
                    else if (isWhite(i) == true)
                    {
                        a = a + ((boardValueNumbers[i]) * 3);
                    }
                    else if (isBlack(i) == true)
                    {
                        a = a + ((boardValueNumbers[i]) * 2);
                    }
                }



            }
            a = a % modNumber;
            hvalue = a;
            return a;
        }
        public void drawBoard()
        {
            int a = 1;
            bool empty = true;
            for (int i = 0; i < totalSquareNum; i++)
            {
                a = 1;
                a <<= i;
                char space = ' ';
                empty = true;
                for (int j = 0; j < (totalDiffPiece * 2); j++)
                {
                    space = ' ';
                    if ((a & bitboard[j]) > 0)
                    {
                        empty = false;
                        switch (j)
                        {
                            case (int)typesOfPiece.WB:
                                {
                                    space = 'B';
                                    break;
                                }
                            case (int)typesOfPiece.WK:
                                {
                                    space = 'K';
                                    break;
                                }
                            case (int)typesOfPiece.WN:
                                {
                                    space = 'N';
                                    break;
                                }
                            case (int)typesOfPiece.WP:
                                {
                                    space = 'P';
                                    break;
                                }
                            case (int)typesOfPiece.WR:
                                {
                                    space = 'R';
                                    break;
                                }
                            case (int)typesOfPiece.BB:
                                {
                                    space = 'b';
                                    break;
                                }
                            case (int)typesOfPiece.BK:
                                {
                                    space = 'k';
                                    break;
                                }
                            case (int)typesOfPiece.BN:
                                {
                                    space = 'n';
                                    break;
                                }
                            case (int)typesOfPiece.BP:
                                {
                                    space = 'p';
                                    break;
                                }
                            case (int)typesOfPiece.BR:
                                {
                                    space = 'r';
                                    break;
                                }

                        }
                        Console.Write(space);

                    }



                }
                if (empty == true)
                {
                    Console.Write("-");
                }
                if (i % 4 == 3)
                {
                    Console.WriteLine();
                }

            }
            //Console.WriteLine();
            //Console.WriteLine("01234567891111111111");
            //Console.WriteLine("          0123456789");
            Console.WriteLine("E: {0}, H: {1}", evaluateBoard(), hashValue());
            Console.Write("nOM: {0} ", numberOfMove);
            if (numberOfMove % 2 == 0)
                Console.WriteLine("White To Move");
            else
                Console.WriteLine("Black To Move");
        }
        public void drawDebugBoard()
        {
            int a = 1;
            bool empty = true;
            for (int i = 0; i < totalSquareNum; i++)
            {
                a = 1;
                a <<= i;
                char space = ' ';
                empty = true;
                for (int j = 0; j < (totalDiffPiece * 2); j++)
                {
                    space = ' ';
                    if ((a & bitboard[j]) > 0)
                    {
                        empty = false;
                        switch (j)
                        {
                            case (int)typesOfPiece.WB:
                                {
                                    space = 'B';
                                    break;
                                }
                            case (int)typesOfPiece.WK:
                                {
                                    space = 'K';
                                    break;
                                }
                            case (int)typesOfPiece.WN:
                                {
                                    space = 'N';
                                    break;
                                }
                            case (int)typesOfPiece.WP:
                                {
                                    space = 'P';
                                    break;
                                }
                            case (int)typesOfPiece.WR:
                                {
                                    space = 'R';
                                    break;
                                }
                            case (int)typesOfPiece.BB:
                                {
                                    space = 'b';
                                    break;
                                }
                            case (int)typesOfPiece.BK:
                                {
                                    space = 'k';
                                    break;
                                }
                            case (int)typesOfPiece.BN:
                                {
                                    space = 'n';
                                    break;
                                }
                            case (int)typesOfPiece.BP:
                                {
                                    space = 'p';
                                    break;
                                }
                            case (int)typesOfPiece.BR:
                                {
                                    space = 'r';
                                    break;
                                }

                        }
                        Console.Write(space);
                    }
                }
                if (empty == true)
                {
                    Console.Write("-");
                }

            }
            Console.WriteLine();
            Console.WriteLine("01234567891111111111");
            Console.WriteLine("          0123456789");
            Console.WriteLine("E: {0}, H: {1}", evaluateBoard(), hashValue());
            Console.Write("nOM: {0} ", numberOfMove);
            if (numberOfMove % 2 == 0)
                Console.WriteLine("White To Move");
            else
                Console.WriteLine("Black To Move");
        }
        public bool movePiece(int startSquare, int endSquare, typesOfPiece type)
        {
            bool moveMade = false;
            if (type == typesOfPiece.WK || type == typesOfPiece.WR || type == typesOfPiece.BK || type == typesOfPiece.BR)
            {
                if (type == typesOfPiece.WK)
                    wkMoved = true;
                else if (type == typesOfPiece.WR)
                    wrMoved = true;
                else if (type == typesOfPiece.BK)
                    bkMoved = true;
                else if (type == typesOfPiece.BR)
                    brMoved = true;
            }
            if (checkMoveIsLegal(startSquare, endSquare, type))
            {
                if (isEmpty(endSquare) == false)
                {
                    if ((int)type < 5)
                    {
                        for (int i = totalDiffPiece; i < totalDiffPiece * 2; i++)
                        {
                            removePiece(endSquare, (typesOfPiece)i);
                        }
                    }
                    else if ((int)type > 4)
                    {
                        for (int i = 0; i < totalDiffPiece; i++)
                        {
                            removePiece(endSquare, (typesOfPiece)i);
                        }
                    }
                }
                addPiece(endSquare, type);
                removePiece(startSquare, type);
                moveMade = true;
                numberOfMove++;
                lastMoveEndSquare = endSquare;
                lastMoveStartSquare = startSquare;
                lastMovePiece = type;
                //evaluateBoard();
                //hashValue();
            }
            return moveMade;//says if the move was made
        }
        public bool checkKingMoveIsLegal(int startSquare, int endSquare, typesOfPiece type)
        {
            if ((startSquare == 0 && endSquare == 2) || (startSquare == 19 && endSquare == 17))
            {
                return checkChatleMoveIsLegal(startSquare, endSquare, type);
            }
            else if (startSquare % 4 == 3 && startSquare / 4 == 4)//top left corner
            {
                if (startSquare - 1 == endSquare || startSquare - 5 == endSquare || startSquare - 4 == endSquare)
                {
                    return true;
                }
            }
            else if (startSquare % 4 == 0 && startSquare / 4 == 4)//top right corner
            {
                if (startSquare + 1 == endSquare || startSquare - 3 == endSquare || startSquare - 4 == endSquare)
                {
                    return true;
                }
            }
            else if (startSquare % 4 == 3 && startSquare / 4 == 0)//bottom left corner
            {
                if (startSquare + 4 == endSquare || startSquare + 3 == endSquare || startSquare - 1 == endSquare)
                {
                    return true;
                }
            }
            else if (startSquare % 4 == 0 && startSquare / 4 == 0)//bottom right corner
            {
                if (startSquare + 4 == endSquare || startSquare + 5 == endSquare || startSquare + 1 == endSquare)
                {
                    return true;
                }
            }
            else if (startSquare % 4 == 3)//left edge
            {
                if ((startSquare + 4 == endSquare) || (startSquare + 3 == endSquare) || (startSquare - 1 == endSquare) || (startSquare - 5 == endSquare) || (startSquare - 4 == endSquare))
                {
                    return true;
                }
            }
            else if (startSquare % 4 == 0)//right edge
            {
                if ((startSquare + 4 == endSquare) || (startSquare + 5 == endSquare) || (startSquare + 1 == endSquare) || (startSquare - 3 == endSquare) || (startSquare - 4 == endSquare))
                {
                    return true;
                }
            }
            else if (startSquare / 4 == 0)// bottom
            {
                if ((startSquare + 4 == endSquare) || (startSquare + 5 == endSquare) || (startSquare + 1 == endSquare) || (startSquare + 3 == endSquare) || (startSquare - 1 == endSquare))
                {
                    return true;
                }
            }
            else if (startSquare / 4 == 4)//top
            {
                if ((startSquare + 1 == endSquare) || (startSquare - 3 == endSquare) || (startSquare - 4 == endSquare) || (startSquare - 5 == endSquare) || (startSquare - 1 == endSquare))
                {
                    return true;
                }
            }
            else
            {
                if ((startSquare + 4 == endSquare) || (startSquare + 3 == endSquare) || (startSquare - 1 == endSquare) || (startSquare - 5 == endSquare) || (startSquare - 4 == endSquare) || (startSquare - 3 == endSquare) || (startSquare + 1 == endSquare) || (startSquare + 5 == endSquare))
                {
                    return true;
                }
            }
            d.outputError(3, "move is not valid");
            return false;
        }
        public bool checkKnightMoveIsLegal(int startSquare, int endSquare)
        {
            switch (startSquare)
            {
                case 0:
                    {
                        if (endSquare == 6 || endSquare == 9)
                            return true;
                        else
                            return false;
                    }
                case 1:
                    {
                        if (endSquare == 8 || endSquare == 10 || endSquare == 7)
                            return true;
                        else
                            return false;
                    }
                case 2:
                    {
                        if (endSquare == 9 || endSquare == 11 || endSquare == 4)
                            return true;
                        else
                            return false;
                    }
                case 3:
                    {
                        if (endSquare == 10 || endSquare == 5)
                            return true;
                        else
                            return false;
                    }
                case 4:
                    {
                        if (endSquare == 2 || endSquare == 10 || endSquare == 13)
                            return true;
                        else
                            return false;
                    }
                case 5:
                    {
                        if (endSquare == 12 || endSquare == 14 || endSquare == 11 || endSquare == 3)
                            return true;
                        else
                            return false;
                    }
                case 6:
                    {
                        if (endSquare == 0 || endSquare == 8 || endSquare == 15 || endSquare == 13)
                            return true;
                        else
                            return false;
                    }
                case 7:
                    {
                        if (endSquare == 1 || endSquare == 9 || endSquare == 14)
                            return true;
                        else
                            return false;
                    }
                case 8:
                    {
                        if (endSquare == 1 || endSquare == 6 || endSquare == 14 || endSquare == 17)
                            return true;
                        else
                            return false;
                    }
                case 9:
                    {
                        if (endSquare == 16 || endSquare == 18 || endSquare == 15 || endSquare == 7 || endSquare == 2 || endSquare == 0)
                            return true;
                        else
                            return false;
                    }
                case 10:
                    {
                        if (endSquare == 1 || endSquare == 3 || endSquare == 4 || endSquare == 12 || endSquare == 17 || endSquare == 19)
                            return true;
                        else
                            return false;
                    }
                case 11:
                    {
                        if (endSquare == 2 || endSquare == 5 || endSquare == 13 || endSquare == 18)
                            return true;
                        else
                            return false;
                    }
                case 12:
                    {
                        if (endSquare == 5 || endSquare == 10 || endSquare == 18)
                            return true;
                        else
                            return false;
                    }
                case 13:
                    {
                        if (endSquare == 4 || endSquare == 6 || endSquare == 11 || endSquare == 19)
                            return true;
                        else
                            return false;
                    }
                case 14:
                    {
                        if (endSquare == 8 || endSquare == 5 || endSquare == 7 || endSquare == 16)
                            return true;
                        else
                            return false;
                    }
                case 15:
                    {
                        if (endSquare == 6 || endSquare == 9 || endSquare == 17)
                            return true;
                        else
                            return false;
                    }
                case 16:
                    {
                        if (endSquare == 9 || endSquare == 14)
                            return true;
                        else
                            return false;
                    }
                case 17:
                    {
                        if (endSquare == 8 || endSquare == 10 || endSquare == 15)
                            return true;
                        else
                            return false;
                    }
                case 18:
                    {
                        if (endSquare == 12 || endSquare == 9 || endSquare == 11)
                            return true;
                        else
                            return false;
                    }
                case 19:
                    {
                        if (endSquare == 13 || endSquare == 10)
                            return true;
                        else
                            return false;
                    }
                default:
                    {
                        return false;
                    }
            }
        }
        public bool checkBishopMoveIsLegal(int startSquare, int endSquare, typesOfPiece type)
        {
            int a = startSquare;
            if (startSquare < 0 || startSquare > 20)
                return false;
            else
            {
                if (startSquare % 4 == 3 && startSquare / 4 == 4)//TL
                {
                    #region checkBisMoveTL
                    if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a -= 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare % 4 == 0 && startSquare / 4 == 4)//TR
                {
                    #region checkBisMoveTR
                    if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a -= 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
                else if (startSquare % 4 == 3 && startSquare / 4 == 0)//BL
                {
                    #region checkBisMoveBL
                    if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a += 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
                else if (startSquare % 4 == 0 && startSquare / 4 == 0)//BR
                {
                    #region checkBisMoveBR
                    if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a += 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
                else if (startSquare % 4 == 3)//LE
                {
                    #region checkBisMoveLE
                    if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a += 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a -= 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
                else if (startSquare % 4 == 0)//RE
                {
                    #region checkBisMoveRE
                    if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a += 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a -= 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
                else if (startSquare / 4 == 0)//B
                {
                    #region checkBisMoveB
                    if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a += 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a += 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
                else if (startSquare / 4 == 4)//T
                {
                    #region checkBisMoveT
                    if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a -= 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a -= 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }

                else
                {
                    #region checkBisMoveN
                    if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a -= 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) < 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a -= 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 3 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 5 != 0)
                    {
                        a += 3;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 5 == 0 && (endSquare - startSquare) > 0 && (endSquare - startSquare) % 3 != 0)
                    {
                        a += 5;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkBishopMoveIsLegal(a, endSquare, type);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;
                    }
                    #endregion
                }
            }
        }
        public bool checkRookMoveIsLegal(int startSquare, int endSquare, typesOfPiece type,int direction)
        {
            int a = startSquare;
            if (startSquare < 0 || startSquare > 20)
                return false;
            else
            {
                if (startSquare % 4 == 3 && startSquare / 4 == 4)//TL
                {
                    #region checkRookMoveTL
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) < 0 && (direction==3||direction==-1))
                    {
                        direction = 3;
                        a -= 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type,3);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) < 0 && (direction == 2 || direction == -1))
                    {
                        direction = 2;
                        --a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type,2);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare % 4 == 0 && startSquare / 4 == 4)//TR
                {
                    #region checkRookMoveTR
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) < 0 && (direction == 3 || direction == -1))
                    {
                        direction = 3;
                        a -= 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 3);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) > 0 && (direction == 0 || direction == -1))
                    {
                        direction = 0;
                        ++a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 0);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare % 4 == 3 && startSquare / 4 == 0)//BL
                {
                    #region checkRookMoveBL
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) > 0 && (direction == 1 || direction == -1))
                    {
                        direction = 1;
                        a += 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 1);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) < 0 && (direction == 2 || direction == -1))
                    {
                        direction = 2;
                        --a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 2);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare % 4 == 0 && startSquare / 4 == 0)//BR
                {
                    #region checkRookMoveBR
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) > 0 && (direction == 1 || direction == -1))
                    {
                        direction = 1;
                        a += 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 1);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) > 0 && (direction == 0 || direction == -1))
                    {
                        direction = 0;
                        ++a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 0);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare % 4 == 3)//LE
                {
                    #region checkRookMoveLE
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) < 0 && (direction == 3 || direction == -1))
                    {
                        direction = 3;
                        a -= 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 3);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) < 0 && (direction == 2 || direction == -1))
                    {
                        direction = 2;
                        --a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 2);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) > 0 && (direction == 1 || direction == -1))
                    {
                        direction = 1;
                        a += 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 1);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare % 4 == 0)//RE
                {
                    #region checkRookMoveRE
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) < 0 && (direction == 3 || direction == -1))
                    {
                        direction = 3;
                        a -= 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 3);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) > 0 && (direction == 0 || direction == -1))
                    {
                        direction = 0;
                        ++a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 0);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) > 0 && (direction == 1 || direction == -1))
                    {
                        direction = 1;
                        a += 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 1);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare / 4 == 0)//B
                {
                    #region checkRookMoveB
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) > 0 && (direction == 1 || direction == -1))
                    {
                        direction = 1;
                        a += 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 1);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) > 0 && (direction == 0 || direction == -1))
                    {
                        direction = 0;
                        ++a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 0);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) < 0 && (direction == 2 || direction == -1))
                    {
                        direction = 2;
                        --a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 2);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else if (startSquare / 4 == 4)//T
                {
                    #region checkRookMoveT
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) < 0 && (direction == 3 || direction == -1))
                    {
                        direction = 3;
                        a -= 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 3);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) > 0 && (direction == 0 || direction == -1))
                    {
                        direction = 0;
                        ++a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 0);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) < 0 && (direction == 2 || direction == -1))
                    {
                        direction = 2;
                        --a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type, 2);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
                else
                {
                    #region checkRookMoveN
                    if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) < 0 && (direction == 3 || direction == -1))
                    {
                        direction = 3;
                        a -= 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type,3);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) > 0 && (direction == 0 || direction == -1))
                    {
                        direction = 0;
                        ++a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type,0);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 != 0 && (endSquare - startSquare) < 0 && (direction == 2 || direction == -1))
                    {
                        direction = 2;
                        --a;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type,2);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else if ((endSquare - startSquare) % 4 == 0 && (endSquare - startSquare) > 0 && (direction == 1 || direction == -1))
                    {
                        direction = 1;
                        a += 4;
                        if (isEmpty(a))
                        {
                            if (a == endSquare)
                                return true;
                            else
                                return checkRookMoveIsLegal(a, endSquare, type,1);
                        }
                        else
                        {
                            if (isOppositeColour(a, type) && a == endSquare)
                                return true;
                            else if (isSameColour(a, type))
                                return false;
                            else
                                return false;
                        }
                    }
                    else
                    {
                        d.outputError(3, "move is not valid");
                        return false;

                    }
                    #endregion
                }
            }
        }
        public bool checkPawnMoveIsLegal(int startSquare, int endSquare, typesOfPiece type)
        {
            #region checkPawnMoveWhite
            if ((int)type < totalDiffPiece)
            {
                if (startSquare / 4 == 1)//S
                {
                    if (startSquare + 8 == endSquare && isEmpty(startSquare + 4))
                        return true;
                    else if (startSquare + 4 == endSquare)
                        return true;
                    else if (isOppositeColour(startSquare + 5, typesOfPiece.WP) && startSquare + 5 == endSquare)
                        return true;
                    else
                        return false;
                }
                else if (startSquare % 4 == 3)//LE
                {
                    if (startSquare + 4 == endSquare && isEmpty(startSquare + 4))
                        return true;
                    else if (isOppositeColour(startSquare + 5, typesOfPiece.WP) && startSquare + 5 == endSquare)
                        return true;
                    else
                        return false;
                }
                else if (startSquare % 4 == 0)//RE
                {
                    if (startSquare + 4 == endSquare && isEmpty(startSquare + 4))
                        return true;
                    else if (isOppositeColour(startSquare + 3, typesOfPiece.WP) && startSquare + 3 == endSquare)
                        return true;
                    else
                        return false;
                }
                else if (startSquare / 4 == 4)//T (Pro)
                    return false;
                else
                {
                    if (startSquare + 4 == endSquare && isEmpty(startSquare + 4))
                        return true;
                    else if ((isOppositeColour(startSquare + 3, typesOfPiece.WP) && startSquare + 3 == endSquare) || (isOppositeColour(startSquare + 5, typesOfPiece.WP) && startSquare + 5 == endSquare))
                        return true;
                    else
                        return false;
                }
            }
            #endregion
            #region checkPawnMoveBlack
            else
            {
                if (startSquare / 4 == 3)//S
                {
                    if (startSquare - 8 == endSquare && isEmpty(startSquare - 4))
                        return true;
                    else if (startSquare - 4 == endSquare)
                        return true;
                    else if (isOppositeColour(startSquare - 5, typesOfPiece.BP) && startSquare - 5 == endSquare)
                        return true;
                    else
                        return false;
                }
                else if (startSquare % 4 == 3)//LE
                {
                    if (startSquare - 4 == endSquare && isEmpty(startSquare - 4))
                        return true;
                    else if (isOppositeColour(startSquare - 5, typesOfPiece.BP) && startSquare - 5 == endSquare)
                        return true;
                    else
                        return false;
                }
                else if (startSquare % 4 == 0)//RE
                {
                    if (startSquare - 4 == endSquare && isEmpty(startSquare - 4))
                        return true;
                    else if (isOppositeColour(startSquare - 3, typesOfPiece.BP) && startSquare - 3 == endSquare)
                        return true;
                    else
                        return false;
                }
                else if (startSquare / 4 == 0)//B (Pro)
                    return false;
                else
                {
                    if (startSquare - 4 == endSquare && isEmpty(startSquare - 4))
                        return true;
                    else if ((isOppositeColour(startSquare - 3, typesOfPiece.BP) && startSquare - 3 == endSquare) || (isOppositeColour(startSquare - 5, typesOfPiece.BP) && startSquare - 5 == endSquare))
                        return true;
                    else
                        return false;
                }
            }
            #endregion
        }
        public bool isOppositeColour(int squareNum, typesOfPiece type)
        {
            if ((int)type < totalDiffPiece)
            {
                return isBlack(squareNum);
            }
            else
            {
                return isWhite(squareNum);
            }
        }
        public bool isSameColour(int squareNum, typesOfPiece type)
        {
            if ((int)type < totalDiffPiece)
            {
                return isWhite(squareNum);
            }
            else
            {
                return isBlack(squareNum);
            }
        }
        public bool promotePawn(int squareNum, typesOfPiece type)
        {
            if (type == typesOfPiece.WP)
            {
                removePiece(squareNum, typesOfPiece.WP);
                addPiece(squareNum, promotionTypeWhite);
                return true;
            }
            else if (type == typesOfPiece.BP)
            {
                removePiece(squareNum, typesOfPiece.BP);
                addPiece(squareNum, promotionTypeBlack);
                return true;
            }
            else
                return false;
        }//TODO dont add
        public int evaluatePawn()
        {
            int evalPawn = 0;
            bool found = false, pawnPlus4 = true, pawnPlus3 = true, pawnPlus5 = true, pawnPlus8 = true, pawnMinus4 = true, pawnMinus3 = true, pawnMinus5 = true, pawnMinus8 = true;
            int a = 1, shift = 0, pawnSquare = -1;
            #region findPawnSquareWhite
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.WP] & a) > 0)
                {
                    pawnSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalPawnWhite
            pawnPlus4 = checkPawnMoveIsLegal(pawnSquare, pawnSquare + 4, typesOfPiece.WP);
            pawnPlus3 = checkPawnMoveIsLegal(pawnSquare, pawnSquare + 3, typesOfPiece.WP);
            pawnPlus5 = checkPawnMoveIsLegal(pawnSquare, pawnSquare + 5, typesOfPiece.WP);
            pawnPlus8 = checkPawnMoveIsLegal(pawnSquare, pawnSquare + 8, typesOfPiece.WP);
            switch (pawnPlus4)
            {
                case true:
                    {
                        evalPawn++;
                        pawnPlus4 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (pawnPlus3)
            {
                case true:
                    {
                        evalPawn++;
                        pawnPlus3 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (pawnPlus5)
            {
                case true:
                    {
                        evalPawn++;
                        pawnPlus5 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (pawnPlus8)
            {
                case true:
                    {
                        evalPawn++;
                        pawnPlus8 = false;
                        break;
                    }
                case false:
                    break;
            }
            #endregion
            #region findPawnSquareBlack
            a = 1;
            shift = 0;
            pawnSquare = -1;
            found = false;
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.BP] & a) > 0)
                {
                    pawnSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalPawnBlack
            pawnMinus4 = checkPawnMoveIsLegal(pawnSquare, pawnSquare - 4, typesOfPiece.BP);
            pawnMinus3 = checkPawnMoveIsLegal(pawnSquare, pawnSquare - 3, typesOfPiece.BP);
            pawnMinus5 = checkPawnMoveIsLegal(pawnSquare, pawnSquare - 5, typesOfPiece.BP);
            pawnMinus8 = checkPawnMoveIsLegal(pawnSquare, pawnSquare - 8, typesOfPiece.BP);
            switch (pawnMinus4)
            {
                case true:
                    {
                        evalPawn--;
                        pawnMinus4 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (pawnMinus3)
            {
                case true:
                    {
                        evalPawn--;
                        pawnMinus3 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (pawnMinus5)
            {
                case true:
                    {
                        evalPawn--;
                        pawnMinus5 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (pawnMinus8)
            {
                case true:
                    {
                        evalPawn--;
                        pawnMinus8 = false;
                        break;
                    }
                case false:
                    break;
            }
            #endregion
            return evalPawn;
        }
        public int evaluateKing()
        {
            int evalKing = 0;
            bool found = false, kingPlus4 = true, kingPlus3 = true, kingPlus1 = true, kingPlus5 = true, kingMinus4 = true, kingMinus3 = true, kingMinus1 = true, kingMinus5 = true, kingCastle = true;
            int a = 1, shift = 0, kingSquare = -1;
            #region findKingSquareWhite
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.WK] & a) > 0)
                {
                    kingSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalKingWhite
            kingPlus4 = checkKingMoveIsLegal(kingSquare, kingSquare + 4, typesOfPiece.WK);
            kingPlus3 = checkKingMoveIsLegal(kingSquare, kingSquare + 3, typesOfPiece.WK);
            kingPlus5 = checkKingMoveIsLegal(kingSquare, kingSquare + 5, typesOfPiece.WK);
            kingPlus1 = checkKingMoveIsLegal(kingSquare, kingSquare + 1, typesOfPiece.WK);
            kingMinus4 = checkKingMoveIsLegal(kingSquare, kingSquare - 4, typesOfPiece.WK);
            kingMinus3 = checkKingMoveIsLegal(kingSquare, kingSquare - 3, typesOfPiece.WK);
            kingMinus5 = checkKingMoveIsLegal(kingSquare, kingSquare - 5, typesOfPiece.WK);
            kingMinus1 = checkKingMoveIsLegal(kingSquare, kingSquare - 1, typesOfPiece.WK);
            kingCastle = checkKingMoveIsLegal(kingSquare, kingSquare + 2, typesOfPiece.WK);//castle move
            switch (kingPlus4)
            {
                case true:
                    {
                        evalKing++;
                        kingPlus4 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingPlus3)
            {
                case true:
                    {
                        evalKing++;
                        kingPlus3 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingPlus5)
            {
                case true:
                    {
                        evalKing++;
                        kingPlus5 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingPlus1)
            {
                case true:
                    {
                        evalKing++;
                        kingPlus1 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus4)
            {
                case true:
                    {
                        evalKing++;
                        kingMinus4 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus3)
            {
                case true:
                    {
                        evalKing++;
                        kingMinus3 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus5)
            {
                case true:
                    {
                        evalKing++;
                        kingMinus5 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus1)
            {
                case true:
                    {
                        evalKing++;
                        kingMinus1 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingCastle)
            {
                case true:
                    {
                        evalKing++;
                        kingCastle = false;
                        break;
                    }
                case false:
                    break;
            }
            #endregion
            #region findKingSquareBlack
            a = 1;
            shift = 0;
            kingSquare = -1;
            found = false;
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.BK] & a) > 0)
                {
                    kingSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalKingBlack
            kingPlus4 = checkKingMoveIsLegal(kingSquare, kingSquare + 4, typesOfPiece.BK);
            kingPlus3 = checkKingMoveIsLegal(kingSquare, kingSquare + 3, typesOfPiece.BK);
            kingPlus5 = checkKingMoveIsLegal(kingSquare, kingSquare + 5, typesOfPiece.BK);
            kingPlus1 = checkKingMoveIsLegal(kingSquare, kingSquare + 1, typesOfPiece.BK);
            kingMinus4 = checkKingMoveIsLegal(kingSquare, kingSquare - 4, typesOfPiece.BK);
            kingMinus3 = checkKingMoveIsLegal(kingSquare, kingSquare - 3, typesOfPiece.BK);
            kingMinus5 = checkKingMoveIsLegal(kingSquare, kingSquare - 5, typesOfPiece.BK);
            kingMinus1 = checkKingMoveIsLegal(kingSquare, kingSquare - 1, typesOfPiece.BK);
            kingCastle = checkKingMoveIsLegal(kingSquare, kingSquare - 2, typesOfPiece.BK);
            switch (kingPlus4)
            {
                case true:
                    {
                        evalKing--;
                        kingPlus4 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingPlus3)
            {
                case true:
                    {
                        evalKing--;
                        kingPlus3 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingPlus5)
            {
                case true:
                    {
                        evalKing--;
                        kingPlus5 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingPlus1)
            {
                case true:
                    {
                        evalKing--;
                        kingPlus1 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus4)
            {
                case true:
                    {
                        evalKing--;
                        kingMinus4 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus3)
            {
                case true:
                    {
                        evalKing--;
                        kingMinus3 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus5)
            {
                case true:
                    {
                        evalKing--;
                        kingMinus5 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingMinus1)
            {
                case true:
                    {
                        evalKing--;
                        kingMinus1 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (kingCastle)
            {
                case true:
                    {
                        evalKing--;
                        kingCastle = false;
                        break;
                    }
                case false:
                    break;
            }
            #endregion
            return evalKing;
        }
        public int evaluateKnight()
        {
            int evalKnight = 0;
            bool found = false, knightPlus7 = true, knightPlus2 = true, knightPlus6 = true, knightPlus9 = true, knightMinus7 = true, knightMinus2 = true, knightMinus6 = true, knightMinus9 = true;
            int a = 1, shift = 0, knightSquare = -1;
            //+7,+2,+6,+9,-7,-2,-6,-9
            #region findKnightSquareWhite
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.WN] & a) > 0)
                {
                    knightSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalKnightWhite
            knightPlus7 = checkKnightMoveIsLegal(knightSquare, knightSquare + 7); //+7,+6,+9
            knightPlus2 = checkKnightMoveIsLegal(knightSquare, knightSquare + 2);
            knightPlus6 = checkKnightMoveIsLegal(knightSquare, knightSquare + 6);
            knightPlus9 = checkKnightMoveIsLegal(knightSquare, knightSquare + 9);
            knightMinus7 = checkKnightMoveIsLegal(knightSquare, knightSquare - 7);
            knightMinus2 = checkKnightMoveIsLegal(knightSquare, knightSquare - 2);
            knightMinus6 = checkKnightMoveIsLegal(knightSquare, knightSquare - 6);
            knightMinus9 = checkKnightMoveIsLegal(knightSquare, knightSquare - 9);
            switch (knightPlus7)
            {
                case true:
                    {
                        evalKnight++;
                        knightPlus7 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightPlus2)
            {
                case true:
                    {
                        evalKnight++;
                        knightPlus2 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightPlus6)
            {
                case true:
                    {
                        evalKnight++;
                        knightPlus6 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightPlus9)
            {
                case true:
                    {
                        evalKnight++;
                        knightPlus9 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus7)
            {
                case true:
                    {
                        evalKnight++;
                        knightMinus7 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus2)
            {
                case true:
                    {
                        evalKnight++;
                        knightMinus2 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus6)
            {
                case true:
                    {
                        evalKnight++;
                        knightMinus6 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus9)
            {
                case true:
                    {
                        evalKnight++;
                        knightMinus9 = false;
                        break;
                    }
                case false:
                    break;
            }
            #endregion
            #region findKnightSquareBlack
            a = 1;
            shift = 0;
            knightSquare = -1;
            found = false;
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.BN] & a) > 0)
                {
                    knightSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalKnightBlack
            knightPlus7 = checkKnightMoveIsLegal(knightSquare, knightSquare + 7);//-7,-2,-6,-9
            knightPlus2 = checkKnightMoveIsLegal(knightSquare, knightSquare + 2);//CHECK if correct
            knightPlus6 = checkKnightMoveIsLegal(knightSquare, knightSquare + 6);
            knightPlus9 = checkKnightMoveIsLegal(knightSquare, knightSquare + 9);
            knightMinus7 = checkKnightMoveIsLegal(knightSquare, knightSquare - 7);
            knightMinus2 = checkKnightMoveIsLegal(knightSquare, knightSquare - 2);
            knightMinus6 = checkKnightMoveIsLegal(knightSquare, knightSquare - 6);
            knightMinus9 = checkKnightMoveIsLegal(knightSquare, knightSquare - 9);
            switch (knightPlus7)
            {
                case true:
                    {
                        evalKnight--;
                        knightPlus7 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightPlus2)
            {
                case true:
                    {
                        evalKnight--;
                        knightPlus2 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightPlus6)
            {
                case true:
                    {
                        evalKnight--;
                        knightPlus6 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightPlus9)
            {
                case true:
                    {
                        evalKnight--;
                        knightPlus9 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus7)
            {
                case true:
                    {
                        evalKnight--;
                        knightMinus7 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus2)
            {
                case true:
                    {
                        evalKnight--;
                        knightMinus2 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus6)
            {
                case true:
                    {
                        evalKnight--;
                        knightMinus6 = false;
                        break;
                    }
                case false:
                    break;
            }
            switch (knightMinus9)
            {
                case true:
                    {
                        evalKnight--;
                        knightMinus9 = false;
                        break;
                    }
                case false:
                    break;
            }
            #endregion
            return evalKnight;
        }
        public int evaluateBishop()
        {
            int evalBishop = 0;
            bool found = false, legal = true;
            int a = 1, shift = 0, bishopSquare = 0;
            #region findBishopSquareWhite
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.WB] & a) > 0)
                {
                    bishopSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalBishopWhite
            int b = bishopSquare;
            while (legal)//2
            {
                b += 3;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                if (legal)
                    evalBishop++;
            }
            legal = true;
            b = bishopSquare;
            while (legal)//1
            {
                b += 5;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                if (legal)
                    evalBishop++;
            }
            legal = true;
            b = bishopSquare;
            while (legal)
            {
                b -= 3;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                if (legal)
                    evalBishop++;
            }
            legal = true;
            b = bishopSquare;
            while (legal)
            {
                b -= 5;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                if (legal)
                    evalBishop++;
            }
            #endregion
            #region findBishopSquareBlack
            a = 1;
            shift = 0;
            bishopSquare = -1;
            found = false;
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.BB] & a) > 0)
                {
                    bishopSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalBishopBlack
            legal = true;
            b = bishopSquare;
            while (legal)
            {
                b += 3;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                if (legal)
                    evalBishop--;
            }
            legal = true;
            b = bishopSquare;
            while (legal)
            {
                b += 5;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                if (legal)
                    evalBishop--;
            }
            legal = true;
            b = bishopSquare;
            while (legal)
            {
                b -= 3;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                if (legal)
                    evalBishop--;
            }
            legal = true;
            b = bishopSquare;
            while (legal)
            {
                b -= 5;
                legal = checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                if (legal)
                    evalBishop--;
            }
            #endregion
            return evalBishop;
        }
        public int evaluateRook()
        {
            int evalRook = 0;
            bool found = false, legal = true;
            int a = 1, shift = 0, rookSquare = -1;
            #region findRookSquareWhite
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.WR] & a) > 0)
                {
                    rookSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalRookWhite
            int b = rookSquare;
            while (legal)
            {
                b += 4;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR,-1);
                if (legal)
                    evalRook++;
            }
            legal = true;
            b = rookSquare;
            while (legal)
            {
                --b;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR,-1);
                if (legal)
                    evalRook++;
            }
            legal = true;
            b = rookSquare;
            while (legal)
            {
                b -= 4;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR,-1);
                if (legal)
                    evalRook++;
            }
            legal = true;
            b = rookSquare;
            while (legal)
            {
                ++b;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR,-1);
                if (legal)
                    evalRook++;
            }
            #endregion
            #region findRookSquareBlack
            a = 1;
            shift = 0;
            rookSquare = -1;
            found = false;
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)typesOfPiece.BR] & a) > 0)
                {
                    rookSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region evalRookBlack
            legal = true;
            b = rookSquare;
            while (legal)
            {
                b += 4;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR,-1);
                if (legal)
                    evalRook--;
            }
            legal = true;
            b = rookSquare;
            while (legal)
            {
                --b;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR,-1);
                if (legal)
                    evalRook--;
            }
            legal = true;
            b = rookSquare;
            while (legal)
            {
                b -= 4;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR,-1);
                if (legal)
                    evalRook--;
            }
            legal = true;
            b = rookSquare;
            while (legal)
            {
                ++b;
                legal = checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR,-1);
                if (legal)
                    evalRook--;
            }
            #endregion
            return evalRook;
        }
        public bool checkIfKingIsInCheck(typesOfPiece king)
        {
            int kingSquare = -1, knightSquare = -1, pawnSquare = -1, bishopSquare = -1, rookSquare = -1;
            #region findKingSquare
            bool found = false;
            int a = 1, shift = 0;
            while (found == false && (shift <= 20))
            {
                if ((bitboard[(int)king] & a) > 0)
                {
                    kingSquare = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            #endregion
            #region findOppSquaresBlack
            if ((int)king < totalDiffPiece)
            {
                #region findKnightSquareBlack
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.BN] & a) > 0)
                    {
                        knightSquare = shift;
                        found = true;
                    }
                else
                {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                #region findPawnSquareBlack
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.BP] & a) > 0)
                    {
                        pawnSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                #region findBishopSquareBlack
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.BB] & a) > 0)
                    {
                        bishopSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                #region findRookSquareBlack
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.BR] & a) > 0)
                    {
                        rookSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
            }
            #endregion
            #region findOppSquaresWhite
            else if ((int)king >=totalDiffPiece)
            {
                #region findKnightSquareWhite
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.WN] & a) > 0)
                    {
                        knightSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                #region findPawnSquareWhite
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.WP] & a) > 0)
                    {
                        pawnSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                #region findBishopSquareWhite
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.WB] & a) > 0)
                    {
                        bishopSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                #region findRookSquareWhite
                found = false;
                a = 1;
                shift = 0;
                while (found == false && (shift <= 20))
                {
                    if ((bitboard[(int)typesOfPiece.WR] & a) > 0)
                    {
                        rookSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
            }
            #endregion
            #region checkForCheck
            switch (kingSquare)
            {
                case 0:
                    {
                        if (pawnSquare == 5 || knightSquare == 9 || knightSquare == 6 || (bishopSquare == 10 && isEmpty(5)) || (bishopSquare == 15 && isEmpty(10) && isEmpty(5)) || rookSquare == 4 || (rookSquare == 8 && isEmpty(4)) || (rookSquare == 12 && isEmpty(4) && isEmpty(8) || rookSquare == 16 && isEmpty(12) && isEmpty(8) && isEmpty(4)) || rookSquare == 1 || (rookSquare == 2 && isEmpty(1)) || (rookSquare == 3 && isEmpty(2) && isEmpty(1)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 1:
                    {
                        if (pawnSquare == 4 || pawnSquare == 6 || knightSquare == 8 || knightSquare == 10 || knightSquare == 7 || bishopSquare == 4 || bishopSquare == 6 || (bishopSquare == 11 && isEmpty(6)) || rookSquare == 0 || rookSquare == 2 || (rookSquare == 3 && isEmpty(2)) || rookSquare == 5 || (rookSquare == 9 && isEmpty(5)) || (rookSquare == 13 && isEmpty(9) && isEmpty(5)) || (rookSquare == 17 && isEmpty(13) && isEmpty(9) && isEmpty(5)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 2:
                    {
                        if (pawnSquare == 5 || pawnSquare == 7 || knightSquare == 4 || knightSquare == 9 || knightSquare == 11 || bishopSquare == 5 || bishopSquare == 7 || (bishopSquare == 8 && isEmpty(5)) || rookSquare == 3 || rookSquare == 1 || rookSquare == 6 || (rookSquare == 0 && isEmpty(1)) || (rookSquare == 10 && isEmpty(6)) || (rookSquare == 14 && isEmpty(10) && isEmpty(6)) || (rookSquare == 18 && isEmpty(14) && isEmpty(10) && isEmpty(6)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 3:
                    {
                        if (pawnSquare == 6 || knightSquare == 5 || knightSquare == 10 || bishopSquare == 6 || (bishopSquare == 9 && isEmpty(6)) || (bishopSquare == 12 && isEmpty(9) && isEmpty(6)) || rookSquare == 7 || rookSquare == 2 || (rookSquare == 11 && isEmpty(7)) || (rookSquare == 15 && isEmpty(11) && isEmpty(7)) || (rookSquare == 19 && isEmpty(15) && isEmpty(11) && isEmpty(7)) || (rookSquare == 1 && isEmpty(2)) || (rookSquare == 0 && isEmpty(1) && isEmpty(2)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 4:
                    {
                        if (pawnSquare == 9 || pawnSquare == 1 || knightSquare == 2 || knightSquare == 10 || knightSquare == 13 || bishopSquare == 1 || bishopSquare == 9 || (bishopSquare == 14 && isEmpty(9)) || (bishopSquare == 19 && isEmpty(14) && isEmpty(9)) || rookSquare == 0 || rookSquare == 5 || rookSquare == 8 || (rookSquare == 6 && isEmpty(5)) || (rookSquare == 7 && isEmpty(6) && isEmpty(5)) || (rookSquare == 12 && isEmpty(8)) || (rookSquare == 16 && isEmpty(12) && isEmpty(8)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 5:
                    {
                        if (pawnSquare == 0 || pawnSquare == 2 || pawnSquare == 8 || pawnSquare == 10 || knightSquare == 12 || knightSquare == 14 || knightSquare == 11 || knightSquare == 3 || bishopSquare == 0 || bishopSquare == 2 || bishopSquare == 8 || bishopSquare == 10 || (bishopSquare == 15 && isEmpty(10)) || rookSquare == 1 || rookSquare == 4 || rookSquare == 6 || rookSquare == 9 || (rookSquare == 7 && isEmpty(6)) || (rookSquare == 13 && isEmpty(9)) || (rookSquare == 17 && isEmpty(13) && isEmpty(9)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 6:
                    {
                        if (pawnSquare == 1 || pawnSquare == 3 || pawnSquare == 9 || pawnSquare == 11 || knightSquare == 0 || knightSquare == 8 || knightSquare == 13 || knightSquare == 15 || bishopSquare == 1 || bishopSquare == 3 || bishopSquare == 9 || bishopSquare == 11 || (bishopSquare == 12 && isEmpty(9)) || rookSquare == 7 || rookSquare == 2 || rookSquare == 5 || rookSquare == 10 || (rookSquare == 4 && isEmpty(5)) || (rookSquare == 14 && isEmpty(10)) || (rookSquare == 18 && isEmpty(14) && isEmpty(10)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 7:
                    {
                        if (pawnSquare == 10 || pawnSquare == 2 || knightSquare == 1 || knightSquare == 9 || knightSquare == 14 || bishopSquare == 10 || bishopSquare == 2 || (bishopSquare == 13 && isEmpty(10)) || (bishopSquare == 16 && isEmpty(13) && isEmpty(10)) || rookSquare == 3 || rookSquare == 6 || rookSquare == 11 || (rookSquare == 5 && isEmpty(6)) || (rookSquare == 4 && isEmpty(5) && isEmpty(6)) || (rookSquare == 15 && isEmpty(11)) || (rookSquare == 19 && isEmpty(15) && isEmpty(11)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 8:
                    {
                        if (pawnSquare == 5 || pawnSquare == 13 || knightSquare == 17 || knightSquare == 14 || knightSquare == 6 || knightSquare == 1 || bishopSquare == 5 || bishopSquare == 13 || (bishopSquare == 2 && isEmpty(5)) || rookSquare == 4 || rookSquare == 9 || rookSquare == 12 || (rookSquare == 0 && isEmpty(4)) || (rookSquare == 10 && isEmpty(9)) || (rookSquare == 11 && isEmpty(10) && isEmpty(9)) || (rookSquare == 16 && isEmpty(12)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 9:
                    {
                        if (pawnSquare == 4 || pawnSquare == 6 || pawnSquare == 12 || pawnSquare == 14 || knightSquare == 15 || knightSquare == 18 || knightSquare == 16 || knightSquare == 7 || knightSquare == 2 || knightSquare == 0 || bishopSquare == 4 || bishopSquare == 6 || bishopSquare == 12 || bishopSquare == 14 || (bishopSquare == 3 && isEmpty(6)) || (bishopSquare == 19 && isEmpty(14)) || rookSquare == 5 || rookSquare == 10 || rookSquare == 13 || rookSquare == 8 || (rookSquare == 17 && isEmpty(13)) || (rookSquare == 1 && isEmpty(5)) || (rookSquare == 11 && isEmpty(10)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 10:
                    {
                        if (pawnSquare == 5 || pawnSquare == 7 || pawnSquare == 15 || pawnSquare == 13 || knightSquare == 1 || knightSquare == 3 || knightSquare == 4 || knightSquare == 12 || knightSquare == 17 || knightSquare == 19 || bishopSquare == 5 || bishopSquare == 7 || bishopSquare == 13 || bishopSquare == 15 || (bishopSquare == 16 && isEmpty(13)) || (bishopSquare == 0 && isEmpty(5)) || rookSquare == 11 || rookSquare == 6 || rookSquare == 14 || rookSquare == 9 || (rookSquare == 2 && isEmpty(6)) || (rookSquare == 8 && isEmpty(9)) || (rookSquare == 18 && isEmpty(14)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 11:
                    {
                        if (pawnSquare == 14 || pawnSquare == 6 || knightSquare == 18 || knightSquare == 13 || knightSquare == 5 || knightSquare == 2 || bishopSquare == 14 || bishopSquare == 6 || (bishopSquare == 1 && isEmpty(6)) || (bishopSquare == 17 && isEmpty(14)) || rookSquare == 7 || rookSquare == 15 || rookSquare == 10 || (rookSquare == 3 && isEmpty(7)) || (rookSquare == 9 && isEmpty(10)) || (rookSquare == 8 && isEmpty(9) && isEmpty(10)) || (rookSquare == 19 && isEmpty(15)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 12:
                    {
                        if (pawnSquare == 17 || pawnSquare == 9 || knightSquare == 5 || knightSquare == 10 || knightSquare == 18 || bishopSquare == 9 || bishopSquare == 17 || (bishopSquare == 6 && isEmpty(9)) || (bishopSquare == 3 && isEmpty(6) && isEmpty(9)) || rookSquare == 16 || rookSquare == 8 || rookSquare == 13 || (rookSquare == 4 && isEmpty(8)) || (rookSquare == 0 && isEmpty(4) && isEmpty(8)) || (rookSquare == 14 && isEmpty(13)) || (rookSquare == 15 && isEmpty(14) && isEmpty(13)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 13:
                    {
                        if (pawnSquare == 8 || pawnSquare == 10 || pawnSquare == 16 || pawnSquare == 18 || knightSquare == 4 || knightSquare == 6 || knightSquare == 11 || knightSquare == 19 || bishopSquare == 8 || bishopSquare == 10 || bishopSquare == 16 || bishopSquare == 18 || (bishopSquare == 7 && isEmpty(10)) || rookSquare == 17 || rookSquare == 12 || rookSquare == 9 || rookSquare == 14 || (rookSquare == 15 && isEmpty(14)) || (rookSquare == 5 && isEmpty(9)) || (rookSquare == 1 && isEmpty(5) && isEmpty(9)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 14:
                    {
                        if (pawnSquare == 9 || pawnSquare == 11 || pawnSquare == 17 || pawnSquare == 19 || knightSquare == 5 || knightSquare == 7 || knightSquare == 8 || knightSquare == 16 || bishopSquare == 9 || bishopSquare == 11 || bishopSquare == 17 || bishopSquare == 19 || (bishopSquare == 4 && isEmpty(9)) || rookSquare == 18 || rookSquare == 15 || rookSquare == 10 || rookSquare == 13 || (rookSquare == 12 && isEmpty(13)) || (rookSquare == 6 && isEmpty(10)) || (rookSquare == 2 && isEmpty(6) && isEmpty(10)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 15:
                    {
                        if (pawnSquare == 10 || pawnSquare == 18 || knightSquare == 6 || knightSquare == 9 || knightSquare == 17 || bishopSquare == 10 || bishopSquare == 18 || (bishopSquare == 5 && isEmpty(10)) || (bishopSquare == 0 && isEmpty(5) && isEmpty(10)) || rookSquare == 19 || rookSquare == 14 || rookSquare == 11 || (rookSquare == 13 && isEmpty(14)) || (rookSquare == 12 && isEmpty(13) && isEmpty(14)) || (rookSquare == 7 && isEmpty(11)) || (rookSquare == 3 && isEmpty(7) && isEmpty(11)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 16:
                    {
                        if (pawnSquare == 13 || knightSquare == 9 || knightSquare == 14 || bishopSquare == 13 || (bishopSquare == 10 && isEmpty(13)) || (bishopSquare == 7 && isEmpty(10) && isEmpty(13)) || rookSquare == 12 || rookSquare == 17 || (rookSquare == 8 && isEmpty(12)) || (rookSquare == 4 && isEmpty(8) && isEmpty(12)) || (rookSquare == 0 && isEmpty(4) && isEmpty(8) && isEmpty(12)) || (rookSquare == 18 && isEmpty(17)) || (rookSquare == 19 && isEmpty(18) && isEmpty(17)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 17:
                    {
                        if (pawnSquare == 12 || pawnSquare == 14 || knightSquare == 8 || knightSquare == 10 || knightSquare == 15 || bishopSquare == 12 || bishopSquare == 14 || (bishopSquare == 11 && isEmpty(14)) || rookSquare == 16 || rookSquare == 18 || rookSquare == 13 || (rookSquare == 19 && isEmpty(18)) || (rookSquare == 9 && isEmpty(13)) || (rookSquare == 5 && isEmpty(9) && isEmpty(13)) || (rookSquare == 1 && isEmpty(5) && isEmpty(9) && isEmpty(13)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 18:
                    {
                        if (pawnSquare == 13 || pawnSquare == 15 || knightSquare == 12 || knightSquare == 9 || knightSquare == 11 || bishopSquare == 13 || bishopSquare == 15 || (bishopSquare == 8 && isEmpty(13)) || rookSquare == 19 || rookSquare == 17 || rookSquare == 14 || (rookSquare == 16 && isEmpty(17)) || (rookSquare == 10 && isEmpty(14)) || (rookSquare == 6 && isEmpty(10) && isEmpty(14)) || (rookSquare == 2 && isEmpty(6) && isEmpty(10) && isEmpty(14)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                case 19:
                    {
                        if (pawnSquare == 14 || knightSquare == 10 || knightSquare == 13 || bishopSquare == 14 || (bishopSquare == 9 && isEmpty(14)) || (bishopSquare == 4 && isEmpty(9) && isEmpty(14)) || rookSquare == 18 || rookSquare == 15 || (rookSquare == 17 && isEmpty(18)) || (rookSquare == 16 && isEmpty(17) && isEmpty(18)) || (rookSquare == 11 && isEmpty(15)) || (rookSquare == 7 && isEmpty(11) && isEmpty(15)) || (rookSquare == 3 && isEmpty(7) && isEmpty(11) && isEmpty(15)))
                        {
                            #region set kInCheck to true
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = true;
                                return true;
                            }
                            else
                            {
                                bkInCheck = true;
                                return true;
                            }
                            #endregion
                        }
                        else
                        {
                            #region set kInCheck to false
                            if (king == typesOfPiece.WK)
                            {
                                wkInCheck = false;
                                return false;
                            }
                            else
                            {
                                bkInCheck = false;
                                return false;
                            }
                            #endregion
                        }
                    }
                default:
                    {
                        return false;
                    }
                #endregion
            }
        }

        public bool checkChatleMoveIsLegal(int startSquare, int endSquare, typesOfPiece type)
        {
            #region checkKingCastle
            if ((int)type < totalDiffPiece && wkMoved == false && wrMoved == false && startSquare == 0 && endSquare == 2)
            {
                if (isEmpty(1) && isEmpty(2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if ((int)type >= totalDiffPiece && bkMoved == false && brMoved == false && startSquare == 19 && endSquare == 17)
            {
                if (isEmpty(18) && isEmpty(17))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            #endregion
        }
        public bool isKing(int squareNum)
        {
            int a = 1;
            a <<= squareNum;
            if ((bitboard[0] & a) > 0)
            {
                return true;
            }
            else if ((bitboard[5] & a) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //TODO check if the king is in checkmate
    }
}
