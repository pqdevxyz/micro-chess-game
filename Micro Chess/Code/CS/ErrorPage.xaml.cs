﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMicroChess2_GD
{
    public partial class ErrorPage : Page
    {
        private UI ui = new UI();

        public ErrorPage(string errorMsg)
        {
            InitializeComponent();
            gridPage.Background = ui.backgroundWhite;
            back_button.Background = ui.buttonBackgroundDBlue;
            back_button.BorderBrush = ui.outlineBlue;
            back_button.Foreground = ui.backgroundWhite;
            back_button.FontFamily = ui.fontMain;
            back_button.FontSize = ui.fontSizeMedium;
            back_button.HorizontalContentAlignment = ui.horizontalCenter;
            back_button.VerticalContentAlignment = ui.verticalCenter;
            back_button.Content = "Go Back";
            error_label.Background = ui.backgroundWhite;
            error_label.Foreground = ui.backgroundDGray;
            error_label.FontFamily = ui.fontMain;
            error_label.FontSize = ui.fontSizeNormal;
            error_label.HorizontalContentAlignment = ui.horizontalCenter;
            error_label.VerticalContentAlignment = ui.verticalCenter;
            error_label.Content = errorMsg;
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                back_button.Background = ui.buttonBackgroundDBlue;
                error_label.Background = ui.backgroundWhite;
                error_label.Foreground = ui.backgroundDGray;
            }
        }
        public ErrorPage(string errorMsg,string buttonMsg)
        {
            InitializeComponent();
            gridPage.Background = ui.backgroundWhite;
            back_button.Background = ui.buttonBackgroundDBlue;
            back_button.BorderBrush = ui.outlineBlue;
            back_button.Foreground = ui.backgroundWhite;
            back_button.FontFamily = ui.fontMain;
            back_button.FontSize = ui.fontSizeMedium;
            back_button.HorizontalContentAlignment = ui.horizontalCenter;
            back_button.VerticalContentAlignment = ui.verticalCenter;
            back_button.Content = buttonMsg;
            error_label.Background = ui.backgroundWhite;
            error_label.Foreground = ui.backgroundDGray;
            error_label.FontFamily = ui.fontMain;
            error_label.FontSize = ui.fontSizeNormal;
            error_label.HorizontalContentAlignment = ui.horizontalCenter;
            error_label.VerticalContentAlignment = ui.verticalCenter;
            error_label.Content = errorMsg;
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                back_button.Background = ui.buttonBackgroundDBlue;
                error_label.Background = ui.backgroundWhite;
                error_label.Foreground = ui.backgroundDGray;
            }
        }
        private void gridPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                back_button.Background = ui.buttonBackgroundDBlue;
                error_label.Foreground = ui.backgroundDGray;
                error_label.Background = ui.backgroundWhite;
            }
            else
            {
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                back_button.Background = ui.buttonBackgroundDBlue;
                error_label.Foreground = ui.backgroundDGray;
                error_label.Background = ui.backgroundWhite;
            }
        }
        private void back_button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}
