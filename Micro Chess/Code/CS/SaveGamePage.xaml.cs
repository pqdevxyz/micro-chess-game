﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMicroChess2_GD
{
    public partial class SaveGamePage : Page
    {
        private Board board = new Board();
        private UI ui = new UI();
        private DataHandling dataHandling = new DataHandling();

        public SaveGamePage(Board saveBoard)
        {
            InitializeComponent();
            board = saveBoard;
            gridPage.Background = ui.backgroundWhite;
            title.Background = ui.backgroundDGray;
            title.BorderBrush = ui.outlineBlue;
            title.Foreground = ui.backgroundWhite;
            title.FontFamily = ui.fontMain;
            title.FontSize = ui.fontSizeHuge;
            title.HorizontalContentAlignment = ui.horizontalCenter;
            title.VerticalContentAlignment = ui.verticalCenter;
            HC_button.Background = ui.backgroundDGray;
            HC_button.BorderBrush = ui.outlineBlue;
            HC_button.Foreground = ui.backgroundWhite;
            HC_button.FontFamily = ui.fontMain;
            HC_button.FontSize = ui.fontSizeMedium;
            HC_button.HorizontalContentAlignment = ui.horizontalCenter;
            HC_button.VerticalContentAlignment = ui.verticalCenter;
            Back_button.Background = ui.buttonBackgroundDBlue;
            Back_button.BorderBrush = ui.outlineBlue;
            Back_button.Foreground = ui.backgroundWhite;
            Back_button.FontFamily = ui.fontMain;
            Back_button.FontSize = ui.fontSizeNormal;
            Back_button.HorizontalContentAlignment = ui.horizontalCenter;
            Back_button.VerticalContentAlignment = ui.verticalCenter;
            fileName_label.Foreground = ui.backgroundDGray;
            fileName_label.FontFamily = ui.fontMain;
            fileName_label.FontSize = ui.fontSizeNormal;
            password_label.Foreground = ui.backgroundDGray;
            password_label.FontFamily = ui.fontMain;
            password_label.FontSize = ui.fontSizeNormal;
            fileName_box.BorderBrush = ui.outlineBlue;
            fileName_box.Foreground = ui.backgroundDGray;
            fileName_box.FontFamily = ui.fontMain;
            fileName_box.FontSize = ui.fontSizeNormal;
            fileName_box.VerticalContentAlignment = ui.verticalCenter;
            password_box.BorderBrush = ui.outlineBlue;
            password_box.Foreground = ui.backgroundDGray;
            password_box.FontFamily = ui.fontMain;
            password_box.FontSize = ui.fontSizeNormal;
            password_box.VerticalContentAlignment = ui.verticalCenter;
            save_button.Background = ui.backgroundDGray;
            save_button.BorderBrush = ui.outlineBlue;
            save_button.Foreground = ui.backgroundWhite;
            save_button.FontFamily = ui.fontMain;
            save_button.FontSize = ui.fontSizeNormal;
            save_button.HorizontalContentAlignment = ui.horizontalCenter;
            save_button.VerticalContentAlignment = ui.verticalCenter;
            saved_label.FontFamily = ui.fontMain;
            saved_label.FontSize = ui.fontSizeHuge;
            saved_label.HorizontalContentAlignment = ui.horizontalCenter;
            saved_label.VerticalContentAlignment = ui.verticalCenter;
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                Back_button.Background = ui.buttonBackgroundDBlue;
                fileName_label.Foreground = ui.backgroundDGray;
                password_label.Foreground = ui.backgroundDGray;
                fileName_box.Foreground = ui.backgroundDGray;
                password_box.Foreground = ui.backgroundDGray;
                fileName_box.Background = ui.backgroundWhite;
                password_box.Background = ui.backgroundWhite;
                save_button.Background = ui.backgroundDGray;
            }
        }

        private void Back_button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();
        }
        private void HC_button_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled == false)
            {
                AppData.hcEnabled = true;
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                Back_button.Background = ui.buttonBackgroundDBlue;
                fileName_label.Foreground = ui.backgroundDGray;
                password_label.Foreground = ui.backgroundDGray;
                fileName_box.Foreground = ui.backgroundDGray;
                password_box.Foreground = ui.backgroundDGray;
                fileName_box.Background = ui.backgroundWhite;
                password_box.Background = ui.backgroundWhite;
                save_button.Background = ui.backgroundDGray;
            }
            else
            {
                AppData.hcEnabled = false;
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                Back_button.Background = ui.buttonBackgroundDBlue;
                fileName_label.Foreground = ui.backgroundDGray;
                password_label.Foreground = ui.backgroundDGray;
                fileName_box.Foreground = ui.backgroundDGray;
                password_box.Foreground = ui.backgroundDGray;
                fileName_box.Background = ui.backgroundWhite;
                password_box.Background = ui.backgroundWhite;
                save_button.Background = ui.backgroundDGray;
            }
        }
        private void save_button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(fileName_box.Text) || string.IsNullOrWhiteSpace(password_box.Password))
            {
                ErrorPage error = new ErrorPage("Please Enter a Name or Password for the Save Game");
                NavigationService.Navigate(error);
            }
            else
            {
                try
                {
                    dataHandling.writeGameDataToFile(board, AppData.filePath, fileName_box.Text, password_box.Password);
                    saved_label.Foreground = ui.buttonBackgroundRed;
                    saved_label.Content = "Game Saved";
                }
                catch (Exception exception)
                {
                    switch (exception.Message)
                    {
                        case "Illegal characters in path.":
                            {
                                ErrorPage error = new ErrorPage("You have entered an invalid letter in the name");
                                NavigationService.Navigate(error);
                                break;
                            }
                        case "FileStream will not open Win32 devices such as disk partitions and tape drives. Avoid use of \"\\\\.\\\" in the path.":
                            {
                                ErrorPage error = new ErrorPage("You have entered an invalid term in the name");
                                NavigationService.Navigate(error);
                                break;
                            }
                        default:
                            {
                                ErrorPage error = new ErrorPage(exception.Message);
                                NavigationService.Navigate(error);
                                break;
                            }
                    }
                }
            }
        }
        private void gridPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                Back_button.Background = ui.buttonBackgroundDBlue;
                fileName_label.Foreground = ui.backgroundDGray;
                password_label.Foreground = ui.backgroundDGray;
                fileName_box.Foreground = ui.backgroundDGray;
                password_box.Foreground = ui.backgroundDGray;
                fileName_box.Background = ui.backgroundWhite;
                password_box.Background = ui.backgroundWhite;
                save_button.Background = ui.backgroundDGray;
            }
            else
            {
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                Back_button.Background = ui.buttonBackgroundDBlue;
                fileName_label.Foreground = ui.backgroundDGray;
                password_label.Foreground = ui.backgroundDGray;
                fileName_box.Foreground = ui.backgroundDGray;
                password_box.Foreground = ui.backgroundDGray;
                fileName_box.Background = ui.backgroundWhite;
                password_box.Background = ui.backgroundWhite;
                save_button.Background = ui.backgroundDGray;
            }
        }
    }
}

