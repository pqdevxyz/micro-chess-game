﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfMicroChess2_GD
{
    class UI
    {
        public SolidColorBrush backgroundDGray;//Main Background Gray
        public SolidColorBrush backgroundWhite;//Main White
        public SolidColorBrush buttonBackgroundDBlue;//Dark Blue used on buttons
        public SolidColorBrush buttonBackgroundRed;//Red used on some buttons
        public SolidColorBrush buttonBackgroundDev;//Colour of Dev Button
        public SolidColorBrush boardWhite;//White squares of the board
        public SolidColorBrush boardBlack;//Black squares of the board
        public SolidColorBrush outlineBlue;//Blue outline colour
        public SolidColorBrush backgroundHCGreen;//High Contrast Mode Green Background Colour
        public SolidColorBrush backgroundHCBlack;//Black used in High Consrast Mode
        public SolidColorBrush boardHCWhite;//High Contrast Pink for White squares on the Board
        public SolidColorBrush boardHCBlack;//High Contrast Blue for Black squares on the Board
        public SolidColorBrush boardButtonInvis;
        public SolidColorBrush MouseHoverWhite;
        public SolidColorBrush MouseHoverBlack;
        public ImageBrush playerStartSquare;
        public ImageBrush playerEndSquare;
        public ImageBrush WK, WN, WB, WR, WP, BK, BN, BB, BR, BP;
        public ImageBrush aiStartSquare;
        public ImageBrush aiEndSquare;
        public ImageBrush HCplayerStartSquare, HCplayerEndSquare, HCaiStartSquare, HCaiEndSquare;
        public FontFamily fontMain;//Main font used in the program  "Berlin Sans FB Regular"
        public int fontSizeHuge;//Used on the Header
        public int fontSizeNormal;//Used as normal font size                 
        public int fontSizeMedium;//Used where a smaller than normal font is needed
        public int pieceSize;
        public HorizontalAlignment horizontalCenter;
        public VerticalAlignment verticalCenter;
        public Thickness zeroThickness;
        public string highContrastButtonOn;
        public string highContrastButtonOff;
        public Visibility hidden;
        public Visibility shown;

        public UI()
        {
            backgroundDGray = new SolidColorBrush(Color.FromRgb(37, 37, 37));
            backgroundWhite = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            buttonBackgroundDBlue = new SolidColorBrush(Color.FromRgb(0, 42, 70));
            buttonBackgroundRed = new SolidColorBrush(Color.FromRgb(144, 24, 24));
            buttonBackgroundDev = new SolidColorBrush(Color.FromRgb(46, 49, 146));
            boardWhite = new SolidColorBrush(Color.FromRgb(183, 183, 183));
            boardBlack = new SolidColorBrush(Color.FromRgb(100, 100, 100));
            outlineBlue = new SolidColorBrush(Color.FromRgb(0, 114, 188));
            backgroundHCGreen = new SolidColorBrush(Color.FromRgb(6, 255, 0));
            backgroundHCBlack = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            boardHCWhite = new SolidColorBrush(Color.FromRgb(255, 0, 255));
            boardHCBlack = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            fontSizeHuge = 60;
            fontSizeNormal = 30;
            fontSizeMedium = 18;
            fontMain = new FontFamily("Berlin Sans FB Regular");
            horizontalCenter = HorizontalAlignment.Center;
            verticalCenter = VerticalAlignment.Center;
            boardButtonInvis = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));//NOTE argb takes a out of 255
            zeroThickness = new Thickness(0);
            playerStartSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath+"\\Images\\startSquare.png", UriKind.Relative)));
            playerEndSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\endSquare.png", UriKind.Relative)));
            WK = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\WK.png", UriKind.Relative)));
            WN = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\WN.png", UriKind.Relative)));
            WB = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\WB.png", UriKind.Relative)));
            WR = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\WR.png", UriKind.Relative)));
            WP = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\WP.png", UriKind.Relative)));
            BK = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\BK.png", UriKind.Relative)));
            BN = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\BN.png", UriKind.Relative)));
            BB = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\BB.png", UriKind.Relative)));
            BR = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\BR.png", UriKind.Relative)));
            BP = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\BP.png", UriKind.Relative)));
            aiStartSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\aistartSquare.png", UriKind.Relative)));
            aiEndSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\aiendSquare.png", UriKind.Relative)));
            HCplayerStartSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\HCstartSquare.png", UriKind.Relative)));
            HCplayerEndSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\HCendSquare.png", UriKind.Relative)));
            HCaiStartSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\HCaistartSquare.png", UriKind.Relative)));
            HCaiEndSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\HCaiendSquare.png", UriKind.Relative)));
            highContrastButtonOff = "High Contrast Mode";
            highContrastButtonOn = "High Contrast On";
            pieceSize = 100;
            hidden = Visibility.Hidden;
            shown = Visibility.Visible;
            MouseHoverWhite = new SolidColorBrush(Color.FromArgb(200, 200, 200, 200));
            MouseHoverBlack = new SolidColorBrush(Color.FromArgb(200, 128, 128, 128));
        }

        public void highContrastOn()
        {
            backgroundDGray = backgroundHCBlack;
            backgroundWhite = backgroundHCGreen;
            buttonBackgroundDBlue = boardHCBlack;//Blue
            boardBlack = boardHCBlack;//Blue
            boardWhite = boardHCWhite;//Pink
            playerStartSquare = HCplayerStartSquare;
            playerEndSquare = HCplayerEndSquare;
            aiStartSquare = HCaiStartSquare;
            aiEndSquare = HCaiEndSquare;
        }
        public void highContrastOff()
        {
            backgroundDGray = new SolidColorBrush(Color.FromRgb(37, 37, 37));
            backgroundWhite = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            buttonBackgroundDBlue = new SolidColorBrush(Color.FromRgb(0, 42, 70));
            buttonBackgroundRed = new SolidColorBrush(Color.FromRgb(144, 24, 24));
            buttonBackgroundDev = new SolidColorBrush(Color.FromRgb(46, 49, 146));
            boardWhite = new SolidColorBrush(Color.FromRgb(183, 183, 183));
            boardBlack = new SolidColorBrush(Color.FromRgb(100, 100, 100));
            outlineBlue = new SolidColorBrush(Color.FromRgb(0, 114, 188));
            backgroundHCGreen = new SolidColorBrush(Color.FromRgb(6, 255, 0));
            backgroundHCBlack = new SolidColorBrush(Color.FromRgb(0, 0, 0));
            boardHCWhite = new SolidColorBrush(Color.FromRgb(255, 0, 255));
            boardHCBlack = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            playerStartSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\startSquare.png", UriKind.Relative)));
            playerEndSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\endSquare.png", UriKind.Relative)));
            aiStartSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\aistartSquare.png", UriKind.Relative)));
            aiEndSquare = new ImageBrush(new BitmapImage(new Uri(AppData.filePath + "\\Images\\aiendSquare.png", UriKind.Relative)));
        }
        public void movePiece(int squareNum, ref int setTop, ref int setLeft)
        {
            switch (squareNum)
            {
                case 0:
                    {
                        setTop = 400;
                        setLeft = 300;
                        break;
                    }
                case 1:
                    {
                        setTop = 400;
                        setLeft = 200;
                        break;
                    }
                case 2:
                    {
                        setTop = 400;
                        setLeft = 100;
                        break;
                    }
                case 3:
                    {
                        setTop = 400;
                        setLeft = 0;
                        break;
                    }
                case 4:
                    {
                        setTop = 300;
                        setLeft = 300;
                        break;
                    }
                case 5:
                    {
                        setTop = 300;
                        setLeft = 200;
                        break;
                    }
                case 6:
                    {
                        setTop = 300;
                        setLeft = 100;
                        break;
                    }
                case 7:
                    {
                        setTop = 300;
                        setLeft = 0;
                        break;
                    }
                case 8:
                    {
                        setTop = 200;
                        setLeft = 300;
                        break;
                    }
                case 9:
                    {
                        setTop = 200;
                        setLeft = 200;
                        break;
                    }
                case 10:
                    {
                        setTop = 200;
                        setLeft = 100;
                        break;
                    }
                case 11:
                    {
                        setTop = 200;
                        setLeft = 0;
                        break;
                    }
                case 12:
                    {
                        setTop = 100;
                        setLeft = 300;
                        break;
                    }
                case 13:
                    {
                        setTop = 100;
                        setLeft = 200;
                        break;
                    }
                case 14:
                    {
                        setTop = 100;
                        setLeft = 100;
                        break;
                    }
                case 15:
                    {
                        setTop = 100;
                        setLeft = 0;
                        break;
                    }
                case 16:
                    {
                        setTop = 0;
                        setLeft = 300;
                        break;
                    }
                case 17:
                    {
                        setTop = 0;
                        setLeft = 200;
                        break;
                    }
                case 18:
                    {
                        setTop = 0;
                        setLeft = 100;
                        break;
                    }
                case 19:
                    {
                        setTop = 0;
                        setLeft = 0;
                        break;
                    }
                default:
                    {
                        setTop = -100;
                        setLeft = 0;
                        break;
                    }
            }
        }

    }
}
