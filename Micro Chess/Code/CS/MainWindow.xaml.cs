﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMicroChess2_GD
{
    public static class AppData
    {
        public static bool hcEnabled = false;
        public static bool devEnabled = false;
        public static string fileName = "blank";
        public static string password = "password";
        public static bool loadGame = false;
        public static string filePath = AppDomain.CurrentDomain.BaseDirectory;
        public static int? aiDiff = null;
        public static bool? playerIsWhite = null;
        public static bool aiGame = true;
        public static bool firstTimeLoad;
        public static int aiStartSquare, aiEndSquare;
    }
    public partial class MainWindow : NavigationWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowState = WindowState.Maximized;
            WindowStyle = WindowStyle.None;
            ShowsNavigationUI = false;
            Main_Menu mm = new Main_Menu();
            NavigationService.Navigate(mm);
        }
    }
}
