﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Diagnostics;
using System.Windows.Threading;

namespace WpfMicroChess2_GD
{
    public partial class BoardPage : Page
    {
        private Debug d = new Debug("BoardPage");
        private Board board = new Board();
        private UI ui = new UI();
        private Generator generator = new Generator();
        private DataHandling dh = new DataHandling();
        private Stopwatch sw = new Stopwatch();
        private Thread genMoves; 

        private int? startSquare, endSquare;//used for seting the start and end square for a move
        private int setTop, setLeft;//used to move the pieces around the board

        public BoardPage(Board NewBoard)
        {
            InitializeComponent();
            board = NewBoard;
            board.playerIsWhite = (bool)AppData.playerIsWhite;
            generator.PlyLevel = (int)AppData.aiDiff;
            gridPage.Background = ui.backgroundWhite;
            title.Background = ui.backgroundDGray;
            title.BorderBrush = ui.outlineBlue;
            title.Foreground = ui.backgroundWhite;
            title.FontFamily = ui.fontMain;
            title.FontSize = ui.fontSizeHuge;
            title.HorizontalContentAlignment = ui.horizontalCenter;
            title.VerticalContentAlignment = ui.verticalCenter;
            HC_button.Background = ui.backgroundDGray;
            HC_button.BorderBrush = ui.outlineBlue;
            HC_button.Foreground = ui.backgroundWhite;
            HC_button.FontFamily = ui.fontMain;
            HC_button.FontSize = ui.fontSizeMedium;
            HC_button.HorizontalContentAlignment = ui.horizontalCenter;
            HC_button.VerticalContentAlignment = ui.verticalCenter;
            EndGame_button.Background = ui.backgroundDGray;
            EndGame_button.BorderBrush = ui.outlineBlue;
            EndGame_button.Foreground = ui.backgroundWhite;
            EndGame_button.FontFamily = ui.fontMain;
            EndGame_button.FontSize = ui.fontSizeNormal;
            EndGame_button.HorizontalContentAlignment = ui.horizontalCenter;
            EndGame_button.VerticalContentAlignment = ui.verticalCenter;
            SaveGame_button.Background = ui.backgroundDGray;
            SaveGame_button.BorderBrush = ui.outlineBlue;
            SaveGame_button.Foreground = ui.backgroundWhite;
            SaveGame_button.FontFamily = ui.fontMain;
            SaveGame_button.FontSize = ui.fontSizeNormal;
            SaveGame_button.HorizontalContentAlignment = ui.horizontalCenter;
            SaveGame_button.VerticalContentAlignment = ui.verticalCenter;
            Move_button.Background = ui.buttonBackgroundDBlue;
            Move_button.BorderBrush = ui.outlineBlue;
            Move_button.Foreground = ui.backgroundWhite;
            Move_button.FontFamily = ui.fontMain;
            Move_button.FontSize = ui.fontSizeNormal;
            Move_button.HorizontalContentAlignment = ui.horizontalCenter;
            Move_button.VerticalContentAlignment = ui.verticalCenter;
            Dev_button.Background = ui.buttonBackgroundDev;
            Dev_button.BorderBrush = ui.outlineBlue;
            Dev_button.Foreground = ui.backgroundWhite;
            Dev_button.FontFamily = ui.fontMain;
            Dev_button.FontSize = ui.fontSizeNormal;
            Dev_button.HorizontalContentAlignment = ui.horizontalCenter;
            Dev_button.VerticalContentAlignment = ui.verticalCenter;
            r0.Fill = ui.boardWhite;
            r2.Fill = ui.boardWhite;
            r5.Fill = ui.boardWhite;
            r7.Fill = ui.boardWhite;
            r8.Fill = ui.boardWhite;
            r10.Fill = ui.boardWhite;
            r13.Fill = ui.boardWhite;
            r15.Fill = ui.boardWhite;
            r16.Fill = ui.boardWhite;
            r18.Fill = ui.boardWhite;
            r1.Fill = ui.boardBlack;
            r3.Fill = ui.boardBlack;
            r4.Fill = ui.boardBlack;
            r6.Fill = ui.boardBlack;
            r9.Fill = ui.boardBlack;
            r11.Fill = ui.boardBlack;
            r12.Fill = ui.boardBlack;
            r14.Fill = ui.boardBlack;
            r17.Fill = ui.boardBlack;
            r19.Fill = ui.boardBlack;
            r0.StrokeThickness = 0;
            r1.StrokeThickness = 0;
            r2.StrokeThickness = 0;
            r3.StrokeThickness = 0;
            r4.StrokeThickness = 0;
            r5.StrokeThickness = 0;
            r6.StrokeThickness = 0;
            r7.StrokeThickness = 0;
            r8.StrokeThickness = 0;
            r9.StrokeThickness = 0;
            r10.StrokeThickness = 0;
            r11.StrokeThickness = 0;
            r12.StrokeThickness = 0;
            r13.StrokeThickness = 0;
            r14.StrokeThickness = 0;
            r15.StrokeThickness = 0;
            r16.StrokeThickness = 0;
            r17.StrokeThickness = 0;
            r18.StrokeThickness = 0;
            r19.StrokeThickness = 0;
            b0.Background = ui.boardButtonInvis;
            b0.BorderBrush = ui.boardButtonInvis;
            b0.BorderThickness = ui.zeroThickness;
            b1.Background = ui.boardButtonInvis;
            b1.BorderBrush = ui.boardButtonInvis;
            b1.BorderThickness = ui.zeroThickness;
            b2.Background = ui.boardButtonInvis;
            b2.BorderBrush = ui.boardButtonInvis;
            b2.BorderThickness = ui.zeroThickness;
            b3.Background = ui.boardButtonInvis;
            b3.BorderBrush = ui.boardButtonInvis;
            b3.BorderThickness = ui.zeroThickness;
            b4.Background = ui.boardButtonInvis;
            b4.BorderBrush = ui.boardButtonInvis;
            b4.BorderThickness = ui.zeroThickness;
            b5.Background = ui.boardButtonInvis;
            b5.BorderBrush = ui.boardButtonInvis;
            b5.BorderThickness = ui.zeroThickness;
            b6.Background = ui.boardButtonInvis;
            b6.BorderBrush = ui.boardButtonInvis;
            b6.BorderThickness = ui.zeroThickness;
            b7.Background = ui.boardButtonInvis;
            b7.BorderBrush = ui.boardButtonInvis;
            b7.BorderThickness = ui.zeroThickness;
            b8.Background = ui.boardButtonInvis;
            b8.BorderBrush = ui.boardButtonInvis;
            b8.BorderThickness = ui.zeroThickness;
            b9.Background = ui.boardButtonInvis;
            b9.BorderBrush = ui.boardButtonInvis;
            b9.BorderThickness = ui.zeroThickness;
            b10.Background = ui.boardButtonInvis;
            b10.BorderBrush = ui.boardButtonInvis;
            b10.BorderThickness = ui.zeroThickness;
            b11.Background = ui.boardButtonInvis;
            b11.BorderBrush = ui.boardButtonInvis;
            b11.BorderThickness = ui.zeroThickness;
            b12.Background = ui.boardButtonInvis;
            b12.BorderBrush = ui.boardButtonInvis;
            b12.BorderThickness = ui.zeroThickness;
            b13.Background = ui.boardButtonInvis;
            b13.BorderBrush = ui.boardButtonInvis;
            b13.BorderThickness = ui.zeroThickness;
            b14.Background = ui.boardButtonInvis;
            b14.BorderBrush = ui.boardButtonInvis;
            b14.BorderThickness = ui.zeroThickness;
            b15.Background = ui.boardButtonInvis;
            b15.BorderBrush = ui.boardButtonInvis;
            b15.BorderThickness = ui.zeroThickness;
            b16.Background = ui.boardButtonInvis;
            b16.BorderBrush = ui.boardButtonInvis;
            b16.BorderThickness = ui.zeroThickness;
            b17.Background = ui.boardButtonInvis;
            b17.BorderBrush = ui.boardButtonInvis;
            b17.BorderThickness = ui.zeroThickness;
            b18.Background = ui.boardButtonInvis;
            b18.BorderBrush = ui.boardButtonInvis;
            b18.BorderThickness = ui.zeroThickness;
            b19.Background = ui.boardButtonInvis;
            b19.BorderBrush = ui.boardButtonInvis;
            b19.BorderThickness = ui.zeroThickness;
            rWK.Height = ui.pieceSize;
            rWK.Width = ui.pieceSize;
            rWK.Fill = ui.WK;
            rWB.Height = ui.pieceSize;
            rWB.Width = ui.pieceSize;
            rWB.Fill = ui.WB;
            rWN.Height = ui.pieceSize;
            rWN.Width = ui.pieceSize;
            rWN.Fill = ui.WN;
            rWP.Height = ui.pieceSize;
            rWP.Width = ui.pieceSize;
            rWP.Fill = ui.WP;
            rWR.Height = ui.pieceSize;
            rWR.Width = ui.pieceSize;
            rWR.Fill = ui.WR;
            rBK.Height = ui.pieceSize;
            rBK.Width = ui.pieceSize;
            rBK.Fill = ui.BK;
            rBB.Height = ui.pieceSize;
            rBB.Width = ui.pieceSize;
            rBB.Fill = ui.BB;
            rBN.Height = ui.pieceSize;
            rBN.Width = ui.pieceSize;
            rBN.Fill = ui.BN;
            rBP.Height = ui.pieceSize;
            rBP.Width = ui.pieceSize;
            rBP.Fill = ui.BP;
            rBR.Height = ui.pieceSize;
            rBR.Width = ui.pieceSize;
            rBR.Fill = ui.BR;
            evalRec.Stroke = ui.outlineBlue;
            whiteEvalRec.Fill = ui.boardWhite;
            blackEvalRec.Fill = ui.boardBlack;
            whiteEvalRec.Visibility = ui.hidden;
            blackEvalRec.Visibility = ui.hidden;
            whiteEvalRec.Height = 200;
            blackEvalRec.Height = 200;
            whiteEvalRec.Width = 25;
            blackEvalRec.Width = 25;
            whiteEvalRec.HorizontalAlignment = ui.horizontalCenter;
            blackEvalRec.HorizontalAlignment = ui.horizontalCenter;
            rBoard.Stroke = ui.outlineBlue;
            whosMove.Foreground = ui.backgroundDGray;
            whosMove.FontFamily = ui.fontMain;
            whosMove.FontSize = ui.fontSizeNormal;
            whosMove.HorizontalContentAlignment = ui.horizontalCenter;
            whosMove.VerticalContentAlignment = ui.verticalCenter;

            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                EndGame_button.Background = ui.backgroundDGray;
                SaveGame_button.Background = ui.backgroundDGray;
                Move_button.Background = ui.buttonBackgroundDBlue;
                Dev_button.Background = ui.buttonBackgroundDev;
                r0.Fill = ui.boardWhite;
                r2.Fill = ui.boardWhite;
                r5.Fill = ui.boardWhite;
                r7.Fill = ui.boardWhite;
                r8.Fill = ui.boardWhite;
                r10.Fill = ui.boardWhite;
                r13.Fill = ui.boardWhite;
                r15.Fill = ui.boardWhite;
                r16.Fill = ui.boardWhite;
                r18.Fill = ui.boardWhite;
                r1.Fill = ui.boardBlack;
                r3.Fill = ui.boardBlack;
                r4.Fill = ui.boardBlack;
                r6.Fill = ui.boardBlack;
                r9.Fill = ui.boardBlack;
                r11.Fill = ui.boardBlack;
                r12.Fill = ui.boardBlack;
                r14.Fill = ui.boardBlack;
                r17.Fill = ui.boardBlack;
                r19.Fill = ui.boardBlack;
                whiteEvalRec.Fill = ui.boardWhite;
                blackEvalRec.Fill = ui.boardBlack;
            }
            setPieceToStart();
        }

        private void HC_button_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled == false)
            {
                AppData.hcEnabled = true;
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                EndGame_button.Background = ui.backgroundDGray;
                SaveGame_button.Background = ui.backgroundDGray;
                Move_button.Background = ui.buttonBackgroundDBlue;
                Dev_button.Background = ui.buttonBackgroundDev;
                r0.Fill = ui.boardWhite;
                r2.Fill = ui.boardWhite;
                r5.Fill = ui.boardWhite;
                r7.Fill = ui.boardWhite;
                r8.Fill = ui.boardWhite;
                r10.Fill = ui.boardWhite;
                r13.Fill = ui.boardWhite;
                r15.Fill = ui.boardWhite;
                r16.Fill = ui.boardWhite;
                r18.Fill = ui.boardWhite;
                r1.Fill = ui.boardBlack;
                r3.Fill = ui.boardBlack;
                r4.Fill = ui.boardBlack;
                r6.Fill = ui.boardBlack;
                r9.Fill = ui.boardBlack;
                r11.Fill = ui.boardBlack;
                r12.Fill = ui.boardBlack;
                r14.Fill = ui.boardBlack;
                r17.Fill = ui.boardBlack;
                r19.Fill = ui.boardBlack;
                whiteEvalRec.Fill = ui.boardWhite;
                blackEvalRec.Fill = ui.boardBlack;
            }
            else
            {
                AppData.hcEnabled = false;
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                EndGame_button.Background = ui.backgroundDGray;
                SaveGame_button.Background = ui.backgroundDGray;
                Move_button.Background = ui.buttonBackgroundDBlue;
                Dev_button.Background = ui.buttonBackgroundDev;
                r0.Fill = ui.boardWhite;
                r2.Fill = ui.boardWhite;
                r5.Fill = ui.boardWhite;
                r7.Fill = ui.boardWhite;
                r8.Fill = ui.boardWhite;
                r10.Fill = ui.boardWhite;
                r13.Fill = ui.boardWhite;
                r15.Fill = ui.boardWhite;
                r16.Fill = ui.boardWhite;
                r18.Fill = ui.boardWhite;
                r1.Fill = ui.boardBlack;
                r3.Fill = ui.boardBlack;
                r4.Fill = ui.boardBlack;
                r6.Fill = ui.boardBlack;
                r9.Fill = ui.boardBlack;
                r11.Fill = ui.boardBlack;
                r12.Fill = ui.boardBlack;
                r14.Fill = ui.boardBlack;
                r17.Fill = ui.boardBlack;
                r19.Fill = ui.boardBlack;
                whiteEvalRec.Fill = ui.boardWhite;
                blackEvalRec.Fill = ui.boardBlack;
            }
        }
        private void EndGame_button_Click(object sender, RoutedEventArgs e)
        {
            Main_Menu main = new Main_Menu();
            NavigationService.Navigate(main);
        }
        private void SaveGame_button_Click(object sender, RoutedEventArgs e)
        {
            SaveGamePage sg = new SaveGamePage(board);
            NavigationService.Navigate(sg);
        }
        private void Move_button_Click(object sender, RoutedEventArgs e)
        {
            movePiece();
            cleanBoard();
            clearMarkers();
            checkGameOver();
            if(AppData.aiGame)
            {
                genMoves = new Thread(startAiGen);
                genMoves.Start();
                while (genMoves.IsAlive)
                {
                    whosMove.Content = "Please Wait.  ";
                    pushUIUpdate();
                    Thread.Sleep(1000);
                    whosMove.Content = "Please Wait . ";
                    pushUIUpdate();
                    Thread.Sleep(1000);
                    whosMove.Content = "Please Wait  .";
                    pushUIUpdate();
                    Thread.Sleep(1000);
                }
                setAiMarkers(board.lastMoveStartSquare, board.lastMoveEndSquare);
                drawBoard();
            }
            checkGameOver();
        }
        private void Dev_button_Click(object sender, RoutedEventArgs e)
        {
            devValues();
            if (!AppData.devEnabled)
            {
                AppData.devEnabled = true;
                devBitBB.Visibility = ui.shown;
                devBitBK.Visibility = ui.shown;
                devBitBN.Visibility = ui.shown;
                devBitBP.Visibility = ui.shown;
                devBitBR.Visibility = ui.shown;
                devBitWB.Visibility = ui.shown;
                devBitWK.Visibility = ui.shown;
                devBitWN.Visibility = ui.shown;
                devBitWP.Visibility = ui.shown;
                devBitWR.Visibility = ui.shown;
                devBKInCheck.Visibility = ui.shown;
                devBKMoved.Visibility = ui.shown;
                devBRMoved.Visibility = ui.shown;
                devEval.Visibility = ui.shown;
                devGameOn.Visibility = ui.shown;
                devHvalue.Visibility = ui.shown;
                devNumberOfMoves.Visibility = ui.shown;
                devWKInCheck.Visibility = ui.shown;
                devWKMoved.Visibility = ui.shown;
                devWRMoved.Visibility = ui.shown;
            }
            else
            {
                AppData.devEnabled = false;
                devBitBB.Visibility = ui.hidden;
                devBitBK.Visibility = ui.hidden;
                devBitBN.Visibility = ui.hidden;
                devBitBP.Visibility = ui.hidden;
                devBitBR.Visibility = ui.hidden;
                devBitWB.Visibility = ui.hidden;
                devBitWK.Visibility = ui.hidden;
                devBitWN.Visibility = ui.hidden;
                devBitWP.Visibility = ui.hidden;
                devBitWR.Visibility = ui.hidden;
                devBKInCheck.Visibility = ui.hidden;
                devBKMoved.Visibility = ui.hidden;
                devBRMoved.Visibility = ui.hidden;
                devEval.Visibility = ui.hidden;
                devGameOn.Visibility = ui.hidden;
                devHvalue.Visibility = ui.hidden;
                devNumberOfMoves.Visibility = ui.hidden;
                devWKInCheck.Visibility = ui.hidden;
                devWKMoved.Visibility = ui.hidden;
                devWRMoved.Visibility = ui.hidden;
            }
        }
        private void b8_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 8;
                b8.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 8 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 8;
                b8.Background = ui.playerEndSquare;
            }
        }
        private void b9_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 9;
                b9.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 9 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 9;
                b9.Background = ui.playerEndSquare;
            }
        }
        private void b10_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 10;
                b10.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 10 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 10;
                b10.Background = ui.playerEndSquare;
            }
        }
        private void b11_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 11;
                b11.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 11 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 11;
                b11.Background = ui.playerEndSquare;
            }
        }
        private void b12_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 12;
                b12.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 12 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 12;
                b12.Background = ui.playerEndSquare;
            }
        }
        private void b13_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 13;
                b13.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 13 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 13;
                b13.Background = ui.playerEndSquare;
            }
        }
        private void b14_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 14;
                b14.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 14 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 14;
                b14.Background = ui.playerEndSquare;
            }
        }
        private void b15_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 15;
                b15.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 15 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 15;
                b15.Background = ui.playerEndSquare;
            }
        }
        private void b16_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 16;
                b16.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 16 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 16;
                b16.Background = ui.playerEndSquare;
            }
        }
        private void b17_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 17;
                b17.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 17 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 17;
                b17.Background = ui.playerEndSquare;
            }
        }
        private void b18_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 18;
                b18.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 18 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 18;
                b18.Background = ui.playerEndSquare;
            }
        }
        private void b19_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 19;
                b19.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 19 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 19;
                b19.Background = ui.playerEndSquare;
            }
        }
        private void b0_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 0;
                b0.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 0 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 0;
                b0.Background = ui.playerEndSquare;
            }
        }
        private void b1_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 1;
                b1.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 1 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 1;
                b1.Background = ui.playerEndSquare;
            }
        }
        private void b2_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 2;
                b2.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 2 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 2;
                b2.Background = ui.playerEndSquare;
            }

        }
        private void b3_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 3;
                b3.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 3 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 3;
                b3.Background = ui.playerEndSquare;
            }
        }
        private void b4_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 4;
                b4.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 4 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 4;
                b4.Background = ui.playerEndSquare;
            }
        }
        private void b5_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 5;
                b5.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 5 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 5;
                b5.Background = ui.playerEndSquare;
            }

        }
        private void b6_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 6;
                b6.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 6 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 6;
                b6.Background = ui.playerEndSquare;
            }
        }
        private void b7_Click(object sender, RoutedEventArgs e)
        {
            if (startSquare == null)
            {
                startSquare = 7;
                b7.Background = ui.playerStartSquare;
            }
            else if ((startSquare != null && endSquare != null) || (startSquare == 7 && endSquare == null))
            {
                startSquare = null;
                endSquare = null;
                clearMarkers();
            }
            else if (startSquare != null && endSquare == null)
            {
                endSquare = 7;
                b7.Background = ui.playerEndSquare;
            }
        }

        private void gridPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (AppData.aiGame)
            {
                if (!(bool)AppData.playerIsWhite && (board.numberOfMove % 2 == 0))
                {
                    generator.getAllMoves(board);
                    generator.findBestMove(false);
                    setAiMarkers(generator.le[generator.MaxPos].bd.lastMoveStartSquare, generator.le[generator.MaxPos].bd.lastMoveEndSquare);
                    generator.makeBestMove(ref board);
                    generator.clearArray();
                    drawBoard();
                }
            }
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                EndGame_button.Background = ui.backgroundDGray;
                SaveGame_button.Background = ui.backgroundDGray;
                Move_button.Background = ui.buttonBackgroundDBlue;
                Dev_button.Background = ui.buttonBackgroundDev;
                r0.Fill = ui.boardWhite;
                r2.Fill = ui.boardWhite;
                r5.Fill = ui.boardWhite;
                r7.Fill = ui.boardWhite;
                r8.Fill = ui.boardWhite;
                r10.Fill = ui.boardWhite;
                r13.Fill = ui.boardWhite;
                r15.Fill = ui.boardWhite;
                r16.Fill = ui.boardWhite;
                r18.Fill = ui.boardWhite;
                r1.Fill = ui.boardBlack;
                r3.Fill = ui.boardBlack;
                r4.Fill = ui.boardBlack;
                r6.Fill = ui.boardBlack;
                r9.Fill = ui.boardBlack;
                r11.Fill = ui.boardBlack;
                r12.Fill = ui.boardBlack;
                r14.Fill = ui.boardBlack;
                r17.Fill = ui.boardBlack;
                r19.Fill = ui.boardBlack;
                whiteEvalRec.Fill = ui.boardWhite;
                blackEvalRec.Fill = ui.boardBlack;
            }
            else
            {
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                EndGame_button.Background = ui.backgroundDGray;
                SaveGame_button.Background = ui.backgroundDGray;
                Move_button.Background = ui.buttonBackgroundDBlue;
                Dev_button.Background = ui.buttonBackgroundDev;
                r0.Fill = ui.boardWhite;
                r2.Fill = ui.boardWhite;
                r5.Fill = ui.boardWhite;
                r7.Fill = ui.boardWhite;
                r8.Fill = ui.boardWhite;
                r10.Fill = ui.boardWhite;
                r13.Fill = ui.boardWhite;
                r15.Fill = ui.boardWhite;
                r16.Fill = ui.boardWhite;
                r18.Fill = ui.boardWhite;
                r1.Fill = ui.boardBlack;
                r3.Fill = ui.boardBlack;
                r4.Fill = ui.boardBlack;
                r6.Fill = ui.boardBlack;
                r9.Fill = ui.boardBlack;
                r11.Fill = ui.boardBlack;
                r12.Fill = ui.boardBlack;
                r14.Fill = ui.boardBlack;
                r17.Fill = ui.boardBlack;
                r19.Fill = ui.boardBlack;
                whiteEvalRec.Fill = ui.boardWhite;
                blackEvalRec.Fill = ui.boardBlack;
            }
        }
        private void setPieceToStart()
        {
            bool found = false;
            int WKS, WNS, WBS, WRS, WPS, BKS, BNS, BBS, BRS, BPS;
            WKS = WNS = WBS = WRS = WPS = BKS = BNS = BBS = BRS = BPS = -1;
            int a = 1, shift = 0;
            if (AppData.loadGame)
            {
                dh.readGameDataFromFile(ref board, AppData.filePath, AppData.fileName, AppData.password);
            }
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.WK] & a) > 0)
                {
                    WKS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.WN] & a) > 0)
                {
                    WNS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.WB] & a) > 0)
                {
                    WBS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.WR] & a) > 0)
                {
                    WRS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.WP] & a) > 0)
                {
                    WPS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.BK] & a) > 0)
                {
                    BKS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.BN] & a) > 0)
                {
                    BNS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.BB] & a) > 0)
                {
                    BBS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.BR] & a) > 0)
                {
                    BRS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            found = false;
            a = 1;
            shift = 0;
            while (!found && (shift <= 20))
            {
                if ((board.bitboard[(int)typesOfPiece.BP] & a) > 0)
                {
                    BPS = shift;
                    found = true;
                }
                else
                {
                    a <<= 1;
                    shift++;
                }
            }
            ui.movePiece(WKS, ref setTop, ref setLeft);
            Canvas.SetTop(rWK, setTop);
            Canvas.SetLeft(rWK, setLeft);
            ui.movePiece(WNS, ref setTop, ref setLeft);
            Canvas.SetTop(rWN, setTop);
            Canvas.SetLeft(rWN, setLeft);
            ui.movePiece(WBS, ref setTop, ref setLeft);
            Canvas.SetTop(rWB, setTop);
            Canvas.SetLeft(rWB, setLeft);
            ui.movePiece(WRS, ref setTop, ref setLeft);
            Canvas.SetTop(rWR, setTop);
            Canvas.SetLeft(rWR, setLeft);
            ui.movePiece(WPS, ref setTop, ref setLeft);
            Canvas.SetTop(rWP, setTop);
            Canvas.SetLeft(rWP, setLeft);
            ui.movePiece(BPS, ref setTop, ref setLeft);
            Canvas.SetTop(rBP, setTop);
            Canvas.SetLeft(rBP, setLeft);
            ui.movePiece(BRS, ref setTop, ref setLeft);
            Canvas.SetTop(rBR, setTop);
            Canvas.SetLeft(rBR, setLeft);
            ui.movePiece(BBS, ref setTop, ref setLeft);
            Canvas.SetTop(rBB, setTop);
            Canvas.SetLeft(rBB, setLeft);
            ui.movePiece(BNS, ref setTop, ref setLeft);
            Canvas.SetTop(rBN, setTop);
            Canvas.SetLeft(rBN, setLeft);
            ui.movePiece(BKS, ref setTop, ref setLeft);
            Canvas.SetTop(rBK, setTop);
            Canvas.SetLeft(rBK, setLeft);
            updateEvalRec();
            updateWhosMove();
            cleanBoard();

        }
        private void clearMarkers()
        {
            b0.Background = ui.boardButtonInvis;
            b1.Background = ui.boardButtonInvis;
            b2.Background = ui.boardButtonInvis;
            b3.Background = ui.boardButtonInvis;
            b4.Background = ui.boardButtonInvis;
            b5.Background = ui.boardButtonInvis;
            b6.Background = ui.boardButtonInvis;
            b7.Background = ui.boardButtonInvis;
            b8.Background = ui.boardButtonInvis;
            b9.Background = ui.boardButtonInvis;
            b10.Background = ui.boardButtonInvis;
            b11.Background = ui.boardButtonInvis;
            b12.Background = ui.boardButtonInvis;
            b13.Background = ui.boardButtonInvis;
            b14.Background = ui.boardButtonInvis;
            b15.Background = ui.boardButtonInvis;
            b16.Background = ui.boardButtonInvis;
            b17.Background = ui.boardButtonInvis;
            b18.Background = ui.boardButtonInvis;
            b19.Background = ui.boardButtonInvis;
            startSquare = null;
            endSquare = null;
        }
        private void movePiece()
        {
            UIElement element = r0;
            bool legal = false;
            int ss, es;
            typesOfPiece type;

            if (startSquare != null && endSquare != null)
            {
                ss = (int)startSquare;
                es = (int)endSquare;
                if (board.pieceOn(ss, typesOfPiece.WK))
                {
                    type = typesOfPiece.WK;
                    legal = board.movePiece(ss, es, type);
                    element = rWK;
                }
                else if (board.pieceOn(ss, typesOfPiece.WB))
                {
                    type = typesOfPiece.WB;
                    legal = board.movePiece(ss, es, type);
                    element = rWB;
                }
                else if (board.pieceOn(ss, typesOfPiece.WN))
                {
                    type = typesOfPiece.WN;
                    legal = board.movePiece(ss, es, type);
                    element = rWN;
                }
                else if (board.pieceOn(ss, typesOfPiece.WP))
                {
                    type = typesOfPiece.WP;
                    legal = board.movePiece(ss, es, type);
                    element = rWP;
                }
                else if (board.pieceOn(ss, typesOfPiece.WR))
                {
                    type = typesOfPiece.WR;
                    legal = board.movePiece(ss, es, type);
                    element = rWR;
                }
                else if (board.pieceOn(ss, typesOfPiece.BB))
                {
                    type = typesOfPiece.BB;
                    legal = board.movePiece(ss, es, type);
                    element = rBB;
                }
                else if (board.pieceOn(ss, typesOfPiece.BK))
                {
                    type = typesOfPiece.BK;
                    legal = board.movePiece(ss, es, type);
                    element = rBK;
                }
                else if (board.pieceOn(ss, typesOfPiece.BN))
                {
                    type = typesOfPiece.BN;
                    legal = board.movePiece(ss, es, type);
                    element = rBN;
                }
                else if (board.pieceOn(ss, typesOfPiece.BP))
                {
                    type = typesOfPiece.BP;
                    legal = board.movePiece(ss, es, type);
                    element = rBP;
                }
                else if (board.pieceOn(ss, typesOfPiece.BR))
                {
                    type = typesOfPiece.BR;
                    legal = board.movePiece(ss, es, type);
                    element = rBR;
                }

                if (legal)
                {
                    ui.movePiece(es, ref setTop, ref setLeft);
                    Canvas.SetTop(element, setTop);
                    Canvas.SetLeft(element, setLeft);
                    devValues();
                    updateEvalRec();
                    updateWhosMove();
                }
                else
                {
                    ErrorPage err = new ErrorPage("Move is not legal, please try again");
                    NavigationService.Navigate(err);
                }
            }
            else
            {
                ErrorPage err = new ErrorPage("Please select both a start and end position");
                NavigationService.Navigate(err);
            }
        }
        private void devValues()
        {
            devBitWK.Content = "BitBoard WK: " + board.bitboard[(int)typesOfPiece.WK];
            devBitWN.Content = "BitBoard WN: " + board.bitboard[(int)typesOfPiece.WN];
            devBitWB.Content = "BitBoard WB: " + board.bitboard[(int)typesOfPiece.WB];
            devBitWR.Content = "BitBoard WR: " + board.bitboard[(int)typesOfPiece.WR];
            devBitWP.Content = "BitBoard WP: " + board.bitboard[(int)typesOfPiece.WP];
            devBitBK.Content = "BitBoard BK: " + board.bitboard[(int)typesOfPiece.BK];
            devBitBN.Content = "BitBoard BN: " + board.bitboard[(int)typesOfPiece.BN];
            devBitBB.Content = "BitBoard BB: " + board.bitboard[(int)typesOfPiece.BB];
            devBitBR.Content = "BitBoard BR: " + board.bitboard[(int)typesOfPiece.BR];
            devBitBP.Content = "BitBoard BP: " + board.bitboard[(int)typesOfPiece.BP];
            devEval.Content = "Board Eval: " + board.Evalue;
            devHvalue.Content = "Board Hvale: " + board.Hvalue;
            devWKMoved.Content = "Board WKMoved: " + board.WKMoved;
            devWRMoved.Content = "Board WRMoved: " + board.WRMoved;
            devBKMoved.Content = "Board BKMoved: " + board.BKMoved;
            devBRMoved.Content = "Board BRMoved: " + board.BRMoved;
            devWKInCheck.Content = "Board WKInCheck: " + board.WKInCheck;
            devBKInCheck.Content = "Board BKInCheck: " + board.BKInCheck;
            devGameOn.Content = "Board gameOn: " + board.GameOn;
            devNumberOfMoves.Content = "Board numberOfMoves: " + board.numberOfMove;
            devBitWK.Foreground = ui.buttonBackgroundDev;
            devBitWN.Foreground = ui.buttonBackgroundDev;
            devBitWB.Foreground = ui.buttonBackgroundDev;
            devBitWR.Foreground = ui.buttonBackgroundDev;
            devBitWP.Foreground = ui.buttonBackgroundDev;
            devBitBK.Foreground = ui.buttonBackgroundDev;
            devBitBN.Foreground = ui.buttonBackgroundDev;
            devBitBB.Foreground = ui.buttonBackgroundDev;
            devBitBR.Foreground = ui.buttonBackgroundDev;
            devBitBP.Foreground = ui.buttonBackgroundDev;
            devEval.Foreground = ui.buttonBackgroundDev;
            devHvalue.Foreground = ui.buttonBackgroundDev;
            devWKMoved.Foreground = ui.buttonBackgroundDev;
            devWRMoved.Foreground = ui.buttonBackgroundDev;
            devBKMoved.Foreground = ui.buttonBackgroundDev;
            devBRMoved.Foreground = ui.buttonBackgroundDev;
            devWKInCheck.Foreground = ui.buttonBackgroundDev;
            devBKInCheck.Foreground = ui.buttonBackgroundDev;
            devGameOn.Foreground = ui.buttonBackgroundDev;
            devNumberOfMoves.Foreground = ui.buttonBackgroundDev;
        }
        private void cleanBoard()
        {
            cleanKing(typesOfPiece.WK);
            cleanKing(typesOfPiece.BK);
            cleanKnight(typesOfPiece.WN);
            cleanKnight(typesOfPiece.BN);
            cleanBishop(typesOfPiece.WB);
            cleanBishop(typesOfPiece.BB);
            cleanRook(typesOfPiece.WR);
            cleanRook(typesOfPiece.BR);
            cleanPawn(typesOfPiece.WP);
            cleanPawn(typesOfPiece.BP);
        }
        private void cleanKing(typesOfPiece type)
        {
            if (type == typesOfPiece.WK)
            {
                if (board.bitboard[(int)typesOfPiece.WK] == 0)
                {
                    rWK.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rWK, -100);
                    Canvas.SetLeft(rWK, 0);
                }
            }
            else if (type == typesOfPiece.BK)
            {
                if (board.bitboard[(int)typesOfPiece.BK] == 0)
                {
                    rBK.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rBK, -100);
                    Canvas.SetLeft(rBK, 0);
                }
            }
        }
        private void cleanKnight(typesOfPiece type)
        {
            if (type == typesOfPiece.WN)
            {
                if (board.bitboard[(int)typesOfPiece.WN] == 0)
                {
                    rWN.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rWN, -100);
                    Canvas.SetLeft(rWN, 0);
                }
            }
            else if (type == typesOfPiece.BN)
            {
                if (board.bitboard[(int)typesOfPiece.BN] == 0)
                {
                    rBN.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rBN, -100);
                    Canvas.SetLeft(rBN, 0);
                }
            }
        }
        private void cleanBishop(typesOfPiece type)
        {
            if (type == typesOfPiece.WB)
            {
                if (board.bitboard[(int)typesOfPiece.WB] == 0)
                {
                    rWB.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rWB, -100);
                    Canvas.SetLeft(rWB, 0);
                }
            }
            else if (type == typesOfPiece.BB)
            {
                if (board.bitboard[(int)typesOfPiece.BB] == 0)
                {
                    rBB.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rBB, -100);
                    Canvas.SetLeft(rBB, 0);
                }
            }
        }
        private void cleanRook(typesOfPiece type)
        {
            if (type == typesOfPiece.WR)
            {
                if (board.bitboard[(int)typesOfPiece.WR] == 0)
                {
                    rWR.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rWR, -100);
                    Canvas.SetLeft(rWR, 0);
                }
            }
            else if (type == typesOfPiece.BR)
            {
                if (board.bitboard[(int)typesOfPiece.BR] == 0)
                {
                    rBR.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rBR, -100);
                    Canvas.SetLeft(rBR, 0);
                }
            }
        }
        private void cleanPawn(typesOfPiece type)
        {
            if (type == typesOfPiece.WP)
            {
                if (board.bitboard[(int)typesOfPiece.WP] == 0)
                {
                    rWP.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rWP, -100);
                    Canvas.SetLeft(rWP, 0);
                }
            }
            else if (type == typesOfPiece.BP)
            {
                if (board.bitboard[(int)typesOfPiece.BP] == 0)
                {
                    rBP.Fill = ui.boardButtonInvis;
                    Canvas.SetTop(rBP, -100);
                    Canvas.SetLeft(rBP, 0);
                }
            }
        }
        private void updateEvalRec()
        {
            if (board.Evalue > 0)
            {
                whiteEvalRec.Visibility = ui.shown;
                whiteEvalRec.Height = board.Evalue;
                Canvas.SetBottom(whiteEvalRec, (Math.Abs(board.Evalue)));
                blackEvalRec.Visibility = ui.hidden;
            }
            else if (board.Evalue < 0)
            {
                blackEvalRec.Visibility = ui.shown;
                blackEvalRec.Height = Math.Abs(board.Evalue);
                Canvas.SetTop(blackEvalRec, (250 - Math.Abs(board.Evalue)));
                whiteEvalRec.Visibility = ui.hidden;
            }
            else
            {
                whiteEvalRec.Visibility = ui.hidden;
                blackEvalRec.Visibility = ui.hidden;
            }
        }
        private void updateWhosMove()
        {
            if (board.numberOfMove % 2 == 0)
            {
                whosMove.Content = "White To Move";
            }
            else
            {
                whosMove.Content = "Black To Move";
            }
        }
        private void setAiMarkers(int aiStartSquare,int aiEndSquare)
        {
            #region aiStart
            switch (aiStartSquare)
            {
                case 0:
                    {
                        b0.Background = ui.aiStartSquare;
                        break;
                    }
                case 1:
                    {
                        b1.Background = ui.aiStartSquare;
                        break;
                    }
                case 2:
                    {
                        b2.Background = ui.aiStartSquare;
                        break;
                    }
                case 3:
                    {
                        b3.Background = ui.aiStartSquare;
                        break;
                    }
                case 4:
                    {
                        b4.Background = ui.aiStartSquare;
                        break;
                    }
                case 5:
                    {
                        b5.Background = ui.aiStartSquare;
                        break;
                    }
                case 6:
                    {
                        b6.Background = ui.aiStartSquare;
                        break;
                    }
                case 7:
                    {
                        b7.Background = ui.aiStartSquare;
                        break;
                    }
                case 8:
                    {
                        b8.Background = ui.aiStartSquare;
                        break;
                    }
                case 9:
                    {
                        b9.Background = ui.aiStartSquare;
                        break;
                    }
                case 10:
                    {
                        b10.Background = ui.aiStartSquare;
                        break;
                    }
                case 11:
                    {
                        b11.Background = ui.aiStartSquare;
                        break;
                    }
                case 12:
                    {
                        b12.Background = ui.aiStartSquare;
                        break;
                    }
                case 13:
                    {
                        b13.Background = ui.aiStartSquare;
                        break;
                    }
                case 14:
                    {
                        b14.Background = ui.aiStartSquare;
                        break;
                    }
                case 15:
                    {
                        b15.Background = ui.aiStartSquare;
                        break;
                    }
                case 16:
                    {
                        b16.Background = ui.aiStartSquare;
                        break;
                    }
                case 17:
                    {
                        b17.Background = ui.aiStartSquare;
                        break;
                    }
                case 18:
                    {
                        b18.Background = ui.aiStartSquare;
                        break;
                    }
                case 19:
                    {
                        b19.Background = ui.aiStartSquare;
                        break;
                    }
            }
            #endregion
            #region aiEnd
            switch (aiEndSquare)
            {
                case 0:
                    {
                        b0.Background = ui.aiEndSquare;
                        break;
                    }
                case 1:
                    {
                        b1.Background = ui.aiEndSquare;
                        break;
                    }
                case 2:
                    {
                        b2.Background = ui.aiEndSquare;
                        break;
                    }
                case 3:
                    {
                        b3.Background = ui.aiEndSquare;
                        break;
                    }
                case 4:
                    {
                        b4.Background = ui.aiEndSquare;
                        break;
                    }
                case 5:
                    {
                        b5.Background = ui.aiEndSquare;
                        break;
                    }
                case 6:
                    {
                        b6.Background = ui.aiEndSquare;
                        break;
                    }
                case 7:
                    {
                        b7.Background = ui.aiEndSquare;
                        break;
                    }
                case 8:
                    {
                        b8.Background = ui.aiEndSquare;
                        break;
                    }
                case 9:
                    {
                        b9.Background = ui.aiEndSquare;
                        break;
                    }
                case 10:
                    {
                        b10.Background = ui.aiEndSquare;
                        break;
                    }
                case 11:
                    {
                        b11.Background = ui.aiEndSquare;
                        break;
                    }
                case 12:
                    {
                        b12.Background = ui.aiEndSquare;
                        break;
                    }
                case 13:
                    {
                        b13.Background = ui.aiEndSquare;
                        break;
                    }
                case 14:
                    {
                        b14.Background = ui.aiEndSquare;
                        break;
                    }
                case 15:
                    {
                        b15.Background = ui.aiEndSquare;
                        break;
                    }
                case 16:
                    {
                        b16.Background = ui.aiEndSquare;
                        break;
                    }
                case 17:
                    {
                        b17.Background = ui.aiEndSquare;
                        break;
                    }
                case 18:
                    {
                        b18.Background = ui.aiEndSquare;
                        break;
                    }
                case 19:
                    {
                        b19.Background = ui.aiEndSquare;
                        break;
                    }
                }
            #endregion
        }
        private void drawBoard()
        {
            /*
                forech uiPiece find square and move uiPiece to that square
            */

            int[] pieces = new int[10];
            bool found = false;
            int shift = 0, a = 1;

            for (int i = 0; i < 10; i++)
            {

                while (!found && (shift <= 20))
                {
                    if ((board.bitboard[i] & a) > 0)
                    {
                        pieces[i] = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                found = false;
                a = 1;
                shift = 0;
            }
            ui.movePiece(pieces[0], ref setTop, ref setLeft);
            Canvas.SetTop(rWK, setTop);
            Canvas.SetLeft(rWK, setLeft);
            ui.movePiece(pieces[1], ref setTop, ref setLeft);
            Canvas.SetTop(rWN, setTop);
            Canvas.SetLeft(rWN, setLeft);
            ui.movePiece(pieces[2], ref setTop, ref setLeft);
            Canvas.SetTop(rWB, setTop);
            Canvas.SetLeft(rWB, setLeft);
            ui.movePiece(pieces[3], ref setTop, ref setLeft);
            Canvas.SetTop(rWR, setTop);
            Canvas.SetLeft(rWR, setLeft);
            ui.movePiece(pieces[4], ref setTop, ref setLeft);
            Canvas.SetTop(rWP, setTop);
            Canvas.SetLeft(rWP, setLeft);
            ui.movePiece(pieces[9], ref setTop, ref setLeft);
            Canvas.SetTop(rBP, setTop);
            Canvas.SetLeft(rBP, setLeft);
            ui.movePiece(pieces[8], ref setTop, ref setLeft);
            Canvas.SetTop(rBR, setTop);
            Canvas.SetLeft(rBR, setLeft);
            ui.movePiece(pieces[7], ref setTop, ref setLeft);
            Canvas.SetTop(rBB, setTop);
            Canvas.SetLeft(rBB, setLeft);
            ui.movePiece(pieces[6], ref setTop, ref setLeft);
            Canvas.SetTop(rBN, setTop);
            Canvas.SetLeft(rBN, setLeft);
            ui.movePiece(pieces[5], ref setTop, ref setLeft);
            Canvas.SetTop(rBK, setTop);
            Canvas.SetLeft(rBK, setLeft);
            updateEvalRec();
            updateWhosMove();
            cleanBoard();
            devValues();

        }
        private void checkGameOver()
        {
            if (board.GameOn)
            {
                if (!AppData.aiGame)
                {
                    if (board.bitboard[(int)typesOfPiece.WK] == 0)
                    {
                        board.GameOn = false;
                        board.whoWon = "Black won!";
                        GameOverPage go = new GameOverPage(board.whoWon);
                        NavigationService.Navigate(go);
                    }
                    if (board.bitboard[(int)typesOfPiece.BK] == 0)
                    {
                        board.GameOn = false;
                        board.whoWon = "White won!";
                        GameOverPage go = new GameOverPage(board.whoWon);
                        NavigationService.Navigate(go);
                    }
                }
                else
                {
                    if ((bool)AppData.playerIsWhite)
                    {
                        if (board.bitboard[(int)typesOfPiece.WK] == 0)
                        {
                            board.GameOn = false;
                            board.whoWon = "The computer won, Sorry you lost better luck next time.";
                            GameOverPage go = new GameOverPage(board.whoWon);
                            NavigationService.Navigate(go);
                        }
                        if (board.bitboard[(int)typesOfPiece.BK] == 0)
                        {
                            board.GameOn = false;
                            board.whoWon = "You won!";
                            GameOverPage go = new GameOverPage(board.whoWon);
                            NavigationService.Navigate(go);
                        }
                    }
                    if (!(bool)AppData.playerIsWhite)
                    {
                        if (board.bitboard[(int)typesOfPiece.WK] == 0)
                        {
                            board.GameOn = false;
                            board.whoWon = "You won!";
                            GameOverPage go = new GameOverPage(board.whoWon);
                            NavigationService.Navigate(go);
                        }
                        if (board.bitboard[(int)typesOfPiece.BK] == 0)
                        {
                            board.GameOn = false;
                            board.whoWon = "The computer won, Sorry you lost better luck next time.";
                            GameOverPage go = new GameOverPage(board.whoWon);
                            NavigationService.Navigate(go);
                        }
                    }
                }
            }
        }
        private void startAiGen()
        {
            if ((bool)AppData.playerIsWhite && (board.numberOfMove % 2 == 1))
            {
                generator.getAllMoves(board);
                generator.findBestMove(true);
                generator.makeBestMove(ref board);
                generator.clearArray();
            }
            if (!(bool)AppData.playerIsWhite && (board.numberOfMove % 2 == 0))
            {
                generator.getAllMoves(board);
                generator.findBestMove(false);
                generator.makeBestMove(ref board);
                generator.clearArray();
            }
        }
        private void pushUIUpdate()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new DispatcherOperationCallback(delegate (object parameter) {
                frame.Continue = false;
                return null;
            }), null);
            Dispatcher.PushFrame(frame);
        }

        public void OnGameOverChange(object sender,EventArgs e)
        {
            GameOverPage go = new GameOverPage(board.whoWon);
            NavigationService.Navigate(go);
        }
    }
}

