﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMicroChess2_GD
{
    public partial class MainMenuSPOptionsPage : Page
    {
        private UI ui = new UI();

        public MainMenuSPOptionsPage()
        {
            InitializeComponent();
            gridPage.Background = ui.backgroundWhite;

            title.Background = ui.backgroundDGray;
            title.BorderBrush = ui.outlineBlue;
            title.Foreground = ui.backgroundWhite;
            title.FontSize = ui.fontSizeHuge;
            title.HorizontalContentAlignment = ui.horizontalCenter;
            title.VerticalContentAlignment = ui.verticalCenter;
            title.FontFamily = ui.fontMain;

            ai.Background = ui.backgroundWhite;
            ai.Foreground = ui.backgroundDGray;
            ai.FontSize = ui.fontSizeNormal;
            ai.HorizontalContentAlignment = ui.horizontalCenter;
            ai.VerticalContentAlignment = ui.verticalCenter;
            ai.FontFamily = ui.fontMain;

            whiteOrBlack.Background = ui.backgroundWhite;
            whiteOrBlack.Foreground = ui.backgroundDGray;
            whiteOrBlack.FontSize = ui.fontSizeNormal;
            whiteOrBlack.HorizontalContentAlignment = ui.horizontalCenter;
            whiteOrBlack.VerticalContentAlignment = ui.verticalCenter;
            whiteOrBlack.FontFamily = ui.fontMain;

            aiEasy_button.Background = ui.backgroundDGray;
            aiEasy_button.BorderBrush = ui.outlineBlue;
            aiEasy_button.Foreground = ui.backgroundWhite;
            aiEasy_button.FontSize = ui.fontSizeNormal;
            aiEasy_button.HorizontalContentAlignment = ui.horizontalCenter;
            aiEasy_button.VerticalContentAlignment = ui.verticalCenter;
            aiEasy_button.FontFamily = ui.fontMain;

            aiMedium_button.Background = ui.backgroundDGray;
            aiMedium_button.BorderBrush = ui.outlineBlue;
            aiMedium_button.Foreground = ui.backgroundWhite;
            aiMedium_button.FontSize = ui.fontSizeNormal;
            aiMedium_button.HorizontalContentAlignment = ui.horizontalCenter;
            aiMedium_button.VerticalContentAlignment = ui.verticalCenter;
            aiMedium_button.FontFamily = ui.fontMain;

            aiHard_button.Background = ui.backgroundDGray;
            aiHard_button.BorderBrush = ui.outlineBlue;
            aiHard_button.Foreground = ui.backgroundWhite;
            aiHard_button.FontSize = ui.fontSizeNormal;
            aiHard_button.HorizontalContentAlignment = ui.horizontalCenter;
            aiHard_button.VerticalContentAlignment = ui.verticalCenter;
            aiHard_button.FontFamily = ui.fontMain;

            white_button.Background = ui.backgroundDGray;
            white_button.BorderBrush = ui.outlineBlue;
            white_button.Foreground = ui.backgroundWhite;
            white_button.FontSize = ui.fontSizeNormal;
            white_button.HorizontalContentAlignment = ui.horizontalCenter;
            white_button.VerticalContentAlignment = ui.verticalCenter;
            white_button.FontFamily = ui.fontMain;

            black_button.Background = ui.backgroundDGray;
            black_button.BorderBrush = ui.outlineBlue;
            black_button.Foreground = ui.backgroundWhite;
            black_button.FontSize = ui.fontSizeNormal;
            black_button.HorizontalContentAlignment = ui.horizontalCenter;
            black_button.VerticalContentAlignment = ui.verticalCenter;
            black_button.FontFamily = ui.fontMain;

            HC_button.Background = ui.backgroundDGray;
            HC_button.BorderBrush = ui.outlineBlue;
            HC_button.Foreground = ui.backgroundWhite;
            HC_button.FontSize = ui.fontSizeMedium;
            HC_button.FontFamily = ui.fontMain;

            Main_button.Background = ui.buttonBackgroundDBlue;
            Main_button.BorderBrush = ui.outlineBlue;
            Main_button.Foreground = ui.backgroundWhite;
            Main_button.FontSize = ui.fontSizeNormal;
            Main_button.FontFamily = ui.fontMain;

            Start_button.Background = ui.backgroundDGray;
            Start_button.BorderBrush = ui.outlineBlue;
            Start_button.Foreground = ui.backgroundWhite;
            Start_button.FontSize = ui.fontSizeNormal;
            Start_button.FontFamily = ui.fontMain;
            Start_button.HorizontalContentAlignment = ui.horizontalCenter;
            Start_button.VerticalContentAlignment = ui.verticalCenter;

            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                ai.Background = ui.backgroundWhite;
                ai.Foreground = ui.backgroundDGray;
                whiteOrBlack.Background = ui.backgroundWhite;
                whiteOrBlack.Foreground = ui.backgroundDGray;
                aiEasy_button.Background = ui.backgroundDGray;
                aiMedium_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
                white_button.Background = ui.backgroundDGray;
                black_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                Main_button.Background = ui.buttonBackgroundDBlue;
                Start_button.Background = ui.backgroundDGray;
            }
        }

        private void Main_button_Click(object sender, RoutedEventArgs e)
        {
            Main_Menu main = new Main_Menu();
            NavigationService.Navigate(main);
        }
        private void HC_button_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled == false)
            {
                AppData.hcEnabled = true;
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                ai.Background = ui.backgroundWhite;
                ai.Foreground = ui.backgroundDGray;
                whiteOrBlack.Background = ui.backgroundWhite;
                whiteOrBlack.Foreground = ui.backgroundDGray;
                aiEasy_button.Background = ui.backgroundDGray;
                aiMedium_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
                white_button.Background = ui.backgroundDGray;
                black_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                Main_button.Background = ui.buttonBackgroundDBlue;
                Start_button.Background = ui.backgroundDGray;
            }
            else
            {
                AppData.hcEnabled = false;
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                ai.Background = ui.backgroundWhite;
                ai.Foreground = ui.backgroundDGray;
                whiteOrBlack.Background = ui.backgroundWhite;
                whiteOrBlack.Foreground = ui.backgroundDGray;
                aiEasy_button.Background = ui.backgroundDGray;
                aiMedium_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
                white_button.Background = ui.backgroundDGray;
                black_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                Main_button.Background = ui.buttonBackgroundDBlue;
                Start_button.Background = ui.backgroundDGray;
            }
        }
        private void aiEasy_button_Click(object sender, RoutedEventArgs e)
        {
            AppData.aiDiff = 2;
            if (aiEasy_button.Background == ui.backgroundDGray)
            {
                aiEasy_button.Background = ui.buttonBackgroundRed;
                aiMedium_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
            }
        }
        private void aiMedium_button_Click(object sender, RoutedEventArgs e)
        {
            AppData.aiDiff = 4;
            if (aiMedium_button.Background == ui.backgroundDGray)
            {
                aiMedium_button.Background = ui.buttonBackgroundRed;
                aiEasy_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
            }
        }
        private void aiHard_button_Click(object sender, RoutedEventArgs e)
        {
            AppData.aiDiff = 6;
            if (aiHard_button.Background == ui.backgroundDGray)
            {
                aiHard_button.Background = ui.buttonBackgroundRed;
                aiEasy_button.Background = ui.backgroundDGray;
                aiMedium_button.Background = ui.backgroundDGray;
            }
        }
        private void white_button_Click(object sender, RoutedEventArgs e)
        {
            AppData.playerIsWhite = true;
            if (white_button.Background == ui.backgroundDGray)
            {
                white_button.Background = ui.buttonBackgroundRed;
                black_button.Background = ui.backgroundDGray;
            }
        }
        private void black_button_Click(object sender, RoutedEventArgs e)
        {
            AppData.playerIsWhite = false;
            if (black_button.Background == ui.backgroundDGray)
            {
                black_button.Background = ui.buttonBackgroundRed;
                white_button.Background = ui.backgroundDGray;
            }
        }
        private void start_button_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.aiDiff != null && AppData.playerIsWhite != null)
            {
                AppData.aiGame = true;
                Board b = new Board();
                BoardPage boardPage = new BoardPage(b);
                b.GameOnChanged += boardPage.OnGameOverChange;
                NavigationService.Navigate(boardPage);
            }
        }
        private void gridPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                ai.Background = ui.backgroundWhite;
                ai.Foreground = ui.backgroundDGray;
                whiteOrBlack.Background = ui.backgroundWhite;
                whiteOrBlack.Foreground = ui.backgroundDGray;
                aiEasy_button.Background = ui.backgroundDGray;
                aiMedium_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
                white_button.Background = ui.backgroundDGray;
                black_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                Main_button.Background = ui.buttonBackgroundDBlue;
                Start_button.Background = ui.backgroundDGray;
            }
            else
            {
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                ai.Background = ui.backgroundWhite;
                ai.Foreground = ui.backgroundDGray;
                whiteOrBlack.Background = ui.backgroundWhite;
                whiteOrBlack.Foreground = ui.backgroundDGray;
                aiEasy_button.Background = ui.backgroundDGray;
                aiMedium_button.Background = ui.backgroundDGray;
                aiHard_button.Background = ui.backgroundDGray;
                white_button.Background = ui.backgroundDGray;
                black_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                Main_button.Background = ui.buttonBackgroundDBlue;
                Start_button.Background = ui.backgroundDGray;
            }
        }
    }
}
