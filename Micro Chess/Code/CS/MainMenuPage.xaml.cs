﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfMicroChess2_GD
{
    public partial class Main_Menu : Page
    {
        private UI ui = new UI();

        public Main_Menu()
        {
            InitializeComponent();
            gridPage.Background = ui.backgroundWhite;

            title.Background = ui.backgroundDGray;
            title.BorderBrush = ui.outlineBlue;
            title.Foreground = ui.backgroundWhite;
            title.FontSize = ui.fontSizeHuge;
            title.HorizontalContentAlignment = ui.horizontalCenter;
            title.VerticalContentAlignment = ui.verticalCenter;
            title.FontFamily = ui.fontMain;

            SP_button.Background = ui.backgroundDGray;
            SP_button.BorderBrush = ui.outlineBlue;
            SP_button.Foreground = ui.backgroundWhite;
            SP_button.FontSize = ui.fontSizeNormal;
            SP_button.FontFamily = ui.fontMain;

            TP_button.Background = ui.backgroundDGray;
            TP_button.BorderBrush = ui.outlineBlue;
            TP_button.Foreground = ui.backgroundWhite;
            TP_button.FontSize = ui.fontSizeNormal;
            TP_button.FontFamily = ui.fontMain;

            Help_button.Background = ui.backgroundDGray;
            Help_button.BorderBrush = ui.outlineBlue;
            Help_button.Foreground = ui.backgroundWhite;
            Help_button.FontSize = ui.fontSizeNormal;
            Help_button.FontFamily = ui.fontMain;

            LoadGame_button.Background = ui.backgroundDGray;
            LoadGame_button.BorderBrush = ui.outlineBlue;
            LoadGame_button.Foreground = ui.backgroundWhite;
            LoadGame_button.FontSize = ui.fontSizeNormal;
            LoadGame_button.FontFamily = ui.fontMain;

            HC_button.Background = ui.backgroundDGray;
            HC_button.BorderBrush = ui.outlineBlue;
            HC_button.Foreground = ui.backgroundWhite;
            HC_button.FontSize = ui.fontSizeMedium;
            HC_button.FontFamily = ui.fontMain;

            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                SP_button.Background = ui.backgroundDGray;
                TP_button.Background = ui.backgroundDGray;
                Help_button.Background = ui.backgroundDGray;
                LoadGame_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
            }
        }
        private void HC_button_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled == false)
            {
                AppData.hcEnabled = true;
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                SP_button.Background = ui.backgroundDGray;
                TP_button.Background = ui.backgroundDGray;
                LoadGame_button.Background = ui.backgroundDGray;
                Help_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
            }
            else
            {
                AppData.hcEnabled = false;
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                SP_button.Background = ui.backgroundDGray;
                TP_button.Background = ui.backgroundDGray;
                LoadGame_button.Background = ui.backgroundDGray;
                Help_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
            }
        }
        private void SP_button_Click(object sender, RoutedEventArgs e)
        {
            MainMenuSPOptionsPage spOpt = new MainMenuSPOptionsPage();
            NavigationService.Navigate(spOpt);
        }
        private void gridPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                SP_button.Background = ui.backgroundDGray;
                TP_button.Background = ui.backgroundDGray;
                LoadGame_button.Background = ui.backgroundDGray;
                Help_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
            }
            else
            {
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                SP_button.Background = ui.backgroundDGray;
                TP_button.Background = ui.backgroundDGray;
                LoadGame_button.Background = ui.backgroundDGray;
                Help_button.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
            }
        }
        private void TP_button_Click(object sender, RoutedEventArgs e)
        {
            Board board = new Board();
            AppData.aiDiff = -1;
            AppData.aiGame = false;
            AppData.playerIsWhite = true;
            AppData.loadGame = false;
            AppData.firstTimeLoad = true;
            BoardPage boardPage = new BoardPage(board);
            board.GameOnChanged += boardPage.OnGameOverChange;
            NavigationService.Navigate(boardPage);
        }
        private void LoadGame_button_Click(object sender, RoutedEventArgs e)
        {
            LoadGamePage lg = new LoadGamePage();
            NavigationService.Navigate(lg);
        }
    }
}
