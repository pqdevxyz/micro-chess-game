﻿using System;

namespace WpfMicroChess2_GD
{
    class Debug
    {
        private string timeNow = DateTime.UtcNow.ToString("HH:mm:ss.fff");
        public Debug(string s)
        {
            System.Diagnostics.Debug.WriteLine("New Debug Class Created in {0} @ {1}", s, timeNow);
        }
        public void outputToWindow(string s)
        {
            //System.Diagnostics.Debug.WriteLine("Class {0} Called @ {1}", s, timeNow);
        }
        public void outputError(int errorNo, string errorMsg)
        {
            System.Diagnostics.Debug.WriteLine("Error {0}: {1}", errorNo, errorMsg);
        }
    }
}
