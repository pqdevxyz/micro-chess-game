﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfMicroChess2_GD
{
    public partial class GameOverPage : Page
    {
        private UI ui = new UI();

        public GameOverPage(string outputMsg)
        {
            InitializeComponent();
            gridPage.Background = ui.backgroundWhite;
            title.Background = ui.backgroundDGray;
            title.BorderBrush = ui.outlineBlue;
            title.Foreground = ui.backgroundWhite;
            title.FontFamily = ui.fontMain;
            title.FontSize = ui.fontSizeHuge;
            title.HorizontalContentAlignment = ui.horizontalCenter;
            title.VerticalContentAlignment = ui.verticalCenter;
            HC_button.Background = ui.backgroundDGray;
            HC_button.BorderBrush = ui.outlineBlue;
            HC_button.Foreground = ui.backgroundWhite;
            HC_button.FontFamily = ui.fontMain;
            HC_button.FontSize = ui.fontSizeMedium;
            HC_button.HorizontalContentAlignment = ui.horizontalCenter;
            HC_button.VerticalContentAlignment = ui.verticalCenter;
            MainMenu_button.Background = ui.buttonBackgroundDBlue;
            MainMenu_button.BorderBrush = ui.outlineBlue;
            MainMenu_button.Foreground = ui.backgroundWhite;
            MainMenu_button.FontFamily = ui.fontMain;
            MainMenu_button.FontSize = ui.fontSizeNormal;
            MainMenu_button.HorizontalContentAlignment = ui.horizontalCenter;
            MainMenu_button.VerticalContentAlignment = ui.verticalCenter;
            GameOver_textblock.Foreground = ui.backgroundDGray;
            GameOver_textblock.FontFamily = ui.fontMain;
            GameOver_textblock.FontSize = ui.fontSizeNormal;
            GameOver_textblock.TextAlignment = TextAlignment.Center;
            GameOver_textblock.TextWrapping = TextWrapping.Wrap;
            GameOver_textblock.Text = outputMsg + " Please help me by taking a minute to fill out a survey of what you thought about the game";
            ThankYou_label.Foreground = ui.backgroundDGray;
            ThankYou_label.FontFamily = ui.fontMain;
            ThankYou_label.FontSize = ui.fontSizeNormal;
            ThankYou_label.HorizontalContentAlignment = ui.horizontalCenter;
            ThankYou_label.VerticalContentAlignment = ui.verticalCenter;
            ThankYou_label.Content = "Thank you for playing Micro Chess";
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                MainMenu_button.Background = ui.buttonBackgroundDBlue;
                GameOver_textblock.Foreground = ui.backgroundDGray;
                ThankYou_label.Foreground = ui.backgroundDGray;
            }
        }

        private void gridPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled)
            {
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                MainMenu_button.Background = ui.buttonBackgroundDBlue;
                GameOver_textblock.Foreground = ui.backgroundDGray;
                ThankYou_label.Foreground = ui.backgroundDGray;
            }
            else
            {
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                MainMenu_button.Background = ui.buttonBackgroundDBlue;
                GameOver_textblock.Foreground = ui.backgroundDGray;
                ThankYou_label.Foreground = ui.backgroundDGray;
            }
        }
        private void HC_button_Click(object sender, RoutedEventArgs e)
        {
            if (AppData.hcEnabled == false)
            {
                AppData.hcEnabled = true;
                ui.highContrastOn();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.buttonBackgroundRed;
                HC_button.Content = ui.highContrastButtonOn;
                MainMenu_button.Background = ui.buttonBackgroundDBlue;
                GameOver_textblock.Foreground = ui.backgroundDGray;
                ThankYou_label.Foreground = ui.backgroundDGray;
            }
            else
            {
                AppData.hcEnabled = false;
                ui.highContrastOff();
                gridPage.Background = ui.backgroundWhite;
                title.Background = ui.backgroundDGray;
                HC_button.Background = ui.backgroundDGray;
                HC_button.Content = ui.highContrastButtonOff;
                MainMenu_button.Background = ui.buttonBackgroundDBlue;
                GameOver_textblock.Foreground = ui.backgroundDGray;
                ThankYou_label.Foreground = ui.backgroundDGray;
            }
        }
        private void MainMenu_button_Click(object sender, RoutedEventArgs e)
        {
            Main_Menu main = new Main_Menu();
            NavigationService.Navigate(main);
        }

    }
}
