﻿using System;

namespace WpfMicroChess2_GD
{
    public class Generator
    {
        private Debug d = new Debug("generator");//creates object used to write to output window in vs
        private DataHandling dh = new DataHandling();
        public ListElements[] le = new ListElements[leArraySize];//creates an array of LE items used to sort new boards in
        public BestMove[] bm = new BestMove[bmArraySize];
        public const int leArraySize = 500000;//number of items in the ListElemet array
        public const int bmArraySize = 500;

        private int plyLevel = 0;//the number of ply the generator will get moves for
        private bool firstMove = true;
        private int maxEval = -1000, maxPos = 0, currentLevel = 0, bmIndex = 1, bestMove = -999;

        public int nextFree = 0;//the next free space in the ListElement array
        public int PlyLevel
        {
            get
            {
                return plyLevel;
            }
            set
            {
                plyLevel = value;
            }
        }
        public int MaxPos
        {
            get
            {
                return maxPos;
            }
        }

        public Generator()
        {
            for (int i = 0; i < bmArraySize; i++)
            {
                bm[i] = new BestMove();     
            }
        }
        public void getMoves(Board board)
        {
            getPawnMoves(board);
            getKingMoves(board);
            getKnightMoves(board);
            getRookMoves(board);
            getBishopMoves(board);
            le[nextFree - 1].next = -1;
        }
        public void getPawnMoves(Board board)
        {
            bool found = false;
            int a = 1, shift = 0, pawnSquare = -1;
            if (board.numberOfMove % 2 == 0)
            {
                #region findPawnSquareWhite
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.WP] & a) > 0)
                    {
                        pawnSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (pawnSquare == -1)
                    return;
                else
                {
                    //+4,+3,+5,+8                                         
                    addToList(board, pawnSquare, pawnSquare + 4, typesOfPiece.WP);
                    addToList(board, pawnSquare, pawnSquare + 3, typesOfPiece.WP);
                    addToList(board, pawnSquare, pawnSquare + 5, typesOfPiece.WP);
                    addToList(board, pawnSquare, pawnSquare + 8, typesOfPiece.WP);
                }
            }
            else
            {
                #region findPawnSquareBlack
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.BP] & a) > 0)
                    {
                        pawnSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (pawnSquare == -1)
                    return;
                else
                {
                    //-4,-3,-5,-8
                    addToList(board, pawnSquare, pawnSquare - 4, typesOfPiece.BP);
                    addToList(board, pawnSquare, pawnSquare - 3, typesOfPiece.BP);
                    addToList(board, pawnSquare, pawnSquare - 5, typesOfPiece.BP);
                    addToList(board, pawnSquare, pawnSquare - 8, typesOfPiece.BP);
                }
            }
        }
        public void getKingMoves(Board board)
        {
            bool found = false;
            int a = 1, shift = 0, kingSquare = -1;
            if (board.numberOfMove % 2 == 0)
            {
                #region findKingSquareWhite
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.WK] & a) > 0)
                    {
                        kingSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (kingSquare == -1)
                    return;
                else
                {
                    //+4,+3,+5,+1,-1,-3,-4,-5,+2 for castle                                      
                    addToList(board, kingSquare, kingSquare + 4, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare + 3, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare + 5, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare + 1, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare - 1, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare - 3, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare - 4, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare - 5, typesOfPiece.WK);
                    addToList(board, kingSquare, kingSquare + 2, typesOfPiece.WK);
                }
            }
            else
            {
                #region findKingSquareBlack
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.BK] & a) > 0)
                    {
                        kingSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (kingSquare == -1)
                    return;
                else
                {
                    //+4,+3,+5,+1,-1,-3,-4,-5,-2 for castle                                         
                    addToList(board, kingSquare, kingSquare + 4, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare + 3, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare + 5, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare + 1, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare - 1, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare - 3, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare - 4, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare - 5, typesOfPiece.BK);
                    addToList(board, kingSquare, kingSquare - 2, typesOfPiece.BK);
                }
            }
        }
        public void getKnightMoves(Board board)
        {
            bool found = false;
            int a = 1, shift = 0, knightSquare = -1;
            if (board.numberOfMove % 2 == 0)
            {
                #region findKnightSquareWhite
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.WN] & a) > 0)
                    {
                        knightSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (knightSquare == -1)
                    return;
                else
                {
                    //+7,+2,+6,+9,-7,-2,-6,-9                                         
                    addToList(board, knightSquare, knightSquare + 7, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare + 2, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare + 6, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare + 9, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare - 7, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare - 2, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare - 6, typesOfPiece.WN);
                    addToList(board, knightSquare, knightSquare - 9, typesOfPiece.WN);
                }
            }
            else
            {
                #region findKnightSquareBlack
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.BN] & a) > 0)
                    {
                        knightSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (knightSquare == -1)
                    return;
                else
                {
                    //+7,+2,+6,+9,-7,-2,-6,-9
                    addToList(board, knightSquare, knightSquare + 7, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare + 2, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare + 6, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare + 9, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare - 7, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare - 2, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare - 6, typesOfPiece.BN);
                    addToList(board, knightSquare, knightSquare - 9, typesOfPiece.BN);
                }
            }
        }
        public void getBishopMoves(Board board)
        {
            bool found = false, legal = true;
            int a = 1, shift = 0, bishopSquare = -1;
            if (board.numberOfMove % 2 == 0)
            {
                #region findBishopSquareWhite
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.WB] & a) > 0)
                    {
                        bishopSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (bishopSquare == -1)
                    return;
                else
                {
                    int b = bishopSquare;
                    while (legal)
                    {
                        b += 3;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                        addToList(board, bishopSquare, b, typesOfPiece.WB);
                    }
                    legal = true;
                    b = bishopSquare;
                    while (legal)
                    {
                        b += 5;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                        addToList(board, bishopSquare, b, typesOfPiece.WB);
                    }
                    legal = true;
                    b = bishopSquare;
                    while (legal)
                    {
                        b -= 3;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                        addToList(board, bishopSquare, b, typesOfPiece.WB);
                    }
                    legal = true;
                    b = bishopSquare;
                    while (legal)
                    {
                        b -= 5;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.WB);
                        addToList(board, bishopSquare, b, typesOfPiece.WB);
                    }


                }
            }
            else
            {
                #region findBishopSquareBlack
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.BB] & a) > 0)
                    {
                        bishopSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (bishopSquare == -1)
                    return;
                else
                {
                    int b = bishopSquare;
                    while (legal)
                    {
                        b += 3;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                        addToList(board, bishopSquare, b, typesOfPiece.BB);
                    }
                    legal = true;
                    b = bishopSquare;
                    while (legal)
                    {
                        b += 5;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                        addToList(board, bishopSquare, b, typesOfPiece.BB);
                    }
                    legal = true;
                    b = bishopSquare;
                    while (legal)
                    {
                        b -= 3;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                        addToList(board, bishopSquare, b, typesOfPiece.BB);
                    }
                    legal = true;
                    b = bishopSquare;
                    while (legal)
                    {
                        b -= 5;
                        legal = board.checkBishopMoveIsLegal(bishopSquare, b, typesOfPiece.BB);
                        addToList(board, bishopSquare, b, typesOfPiece.BB);
                    }
                }
            }
        }
        public void getRookMoves(Board board)
        {
            bool found = false, legal = true;
            int a = 1, shift = 0, rookSquare = -1;
            if (board.numberOfMove % 2 == 0)
            {
                #region findRookSquareWhite
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.WR] & a) > 0)
                    {
                        rookSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (rookSquare == -1)
                    return;
                else
                {
                    int b = rookSquare;
                    while (legal)
                    {
                        b += 4;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.WR);
                    }
                    legal = true;
                    b = rookSquare;
                    while (legal)
                    {
                        --b;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.WR);
                    }
                    legal = true;
                    b = rookSquare;
                    while (legal)
                    {
                        b -= 4;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.WR);
                    }
                    legal = true;
                    b = rookSquare;
                    while (legal)
                    {
                        ++b;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.WR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.WR);
                    }


                }
            }
            else
            {
                #region findRookSquareBlack
                while (found == false && (shift <= 20))
                {
                    if ((board.bitboard[(int)typesOfPiece.BR] & a) > 0)
                    {
                        rookSquare = shift;
                        found = true;
                    }
                    else
                    {
                        a <<= 1;
                        shift++;
                    }
                }
                #endregion
                if (rookSquare == -1)
                    return;
                else
                {
                    int b = rookSquare;
                    while (legal)
                    {
                        b += 4;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.BR);
                    }
                    legal = true;
                    b = rookSquare;
                    while (legal)
                    {
                        --b;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.BR);
                    }
                    legal = true;
                    b = rookSquare;
                    while (legal)
                    {
                        b -= 4;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.BR);
                    }
                    legal = true;
                    b = rookSquare;
                    while (legal)
                    {
                        ++b;
                        legal = board.checkRookMoveIsLegal(rookSquare, b, typesOfPiece.BR, -1);
                        addToList(board, rookSquare, b, typesOfPiece.BR);
                    }
                }
            }
        }
        public void addToList(Board board, int startSquare, int endSquare, typesOfPiece type)
        {
            if (board.checkMoveIsLegal(startSquare, endSquare, type))
            {
                le[nextFree] = new ListElements();
                le[nextFree + 1] = new ListElements();
                board.copyBoard(ref le[nextFree].bd);
                le[nextFree].bd.movePiece(startSquare, endSquare, type);
                le[nextFree].bd.lastMoveWasAi = true;
                le[nextFree].eval = le[nextFree].bd.evaluateBoard();
                //UNCOMMENT dh.writeToSql(le[nextFree].bd, startSquare, endSquare, type);
                if (firstMove)
                {
                    le[nextFree].down = nextFree;
                    le[nextFree].next = nextFree + 1;
                    firstMove = false;
                    nextFree++;
                    currentLevel++;
                }
                else
                {
                    le[nextFree].next = nextFree + 1;
                    nextFree++;
                }
            }
        }//TODO make it so that it can read from sql database
        public void drawMoves()
        {
            for (int i = 0; i < nextFree; i++)
            {
                Console.WriteLine("{0}:     Down:{1} Next:{2} Eval:{3}", i, le[i].down, le[i].next, le[i].eval);
                le[i].bd.drawBoard();
                Console.WriteLine();
            }

        }
        public void getAllMoves(Board board)
        {
            //2 move = 1 ply
            le[0] = new ListElements();
            board.copyBoard(ref le[0].bd);
            le[0].down = 1;
            nextFree++;

            getMoves(board);//0.5 ply
            if(AppData.aiDiff==2&&(bool)AppData.playerIsWhite)
            {
                blackPly2();
            }
            if(AppData.aiDiff==2&&(bool)!AppData.playerIsWhite)
            {
                whitePly2();
            }
            if(AppData.aiDiff==4&&(bool)AppData.playerIsWhite)
            {
                blackPly4();
            }
            if(AppData.aiDiff==4&&(bool)!AppData.playerIsWhite)
            {
                whitePly4();
            }
            if(AppData.aiDiff==6&&(bool)AppData.playerIsWhite)
            {
                blackPly6();
            }
            if(AppData.aiDiff==6&&(bool)!AppData.playerIsWhite)
            {
                whitePly6();
            }
        }
        public void findBestMove(bool aiIsBlack)
        {
            if (aiIsBlack)
            {
                maxEval = 1000;
                maxPos = 0;
                for (int i = 0; i < bmIndex; i++)
                {
                    if (bm[i].eval < maxEval && bm[i].leIndex!=null)
                    {
                        maxEval = bm[i].eval;
                        maxPos = i;
                    }
                }
            }
            if(!aiIsBlack)
            {
                maxEval = -1000;
                maxPos = 0;
                for (int i = 0; i < bmIndex; i++)
                {
                    if (bm[i].eval > maxEval && bm[i].leIndex != null)
                    {
                        maxEval = bm[i].eval;
                        maxPos = i;
                    }
                }
            }
        }
        public void clearArray()
        {
            for (int i = 0; i <= nextFree; i++)
            {
                le[i] = new ListElements();
            }
            for (int i = 0; i <= bmIndex; i++)
            {
                bm[i] = new BestMove();
            }
            nextFree = 0;
            bmIndex = 1;
            firstMove = true;
        }
        public void makeBestMove(ref Board newBoard)
        {
            le[maxPos].bd.copyBoard(ref newBoard);
        }
        public double calulateAvg(int i)
        {
            double total = 0, nodes = 0;
            while (le[i].next != -1)
            {
                total += le[i].bd.Evalue;
                nodes++;
                i++;
            }
            return total / nodes;
        }
        public void blackPly2()
        {
            int i = 1, j = 1;
            double avg1;
            avg1 = calulateAvg(1);
            while (le[i].next != -1)
            {
                if (le[i].eval < avg1)
                {
                    le[i].down = nextFree;
                    getMoves(le[i].bd);
                    j = le[i].down;
                    while (le[j].next != -1)
                    {
                        if (le[j].eval < bestMove)
                        {
                            bm[bmIndex].eval = le[j].eval;
                            bm[bmIndex].leIndex = j;
                            bestMove = le[j].eval;
                        }
                        j = le[j].next;
                    }
                }
                bmIndex++;
                bestMove = 999;
                i = le[i].next;
            }
        }
        public void whitePly2()
        {
            int i = 1, j = 1;
            double avg1;
            avg1 = calulateAvg(1);
            while (le[i].next != -1)
            {
                if (le[i].eval > avg1)
                {
                    le[i].down = nextFree;
                    getMoves(le[i].bd);
                    j = le[i].down;
                    while (le[j].next != -1)
                    {
                        if (le[j].eval > bestMove)
                        {
                            bm[bmIndex].eval = le[j].eval;
                            bm[bmIndex].leIndex = j;
                            bestMove = le[j].eval;
                        }
                        j = le[j].next;
                    }
                }
                bmIndex++;
                bestMove = -999;
                i = le[i].next;
            }
        }
        public void blackPly4()
        {
            int i = 1, j = 1, k = 1, l=1;
            double avg1, avg2,avg3;
            avg1 = calulateAvg(1);
            while (le[i].next != -1)
            {
                if (le[i].eval < avg1)
                {
                    le[i].down = nextFree;
                    getMoves(le[i].bd);
                    avg2 = calulateAvg(i);
                    j = le[i].down;
                    while (le[j].next != -1)
                    {
                        if (le[j].eval < avg2)
                        {
                            le[j].down = nextFree;
                            getMoves(le[j].bd);
                            avg3 = calulateAvg(j);
                            k = le[j].down;
                            while (le[k].next != -1)
                            {
                                if(le[k].eval < avg3)
                                {
                                    le[k].down = nextFree;
                                    getMoves(le[k].bd);
                                    l = le[k].down;
                                    while(le[l].next != -1)
                                    {
                                        if(le[l].eval < bestMove)                                             
                                        {
                                            bm[bmIndex].eval = le[l].eval;
                                            bm[bmIndex].leIndex = l;
                                            bestMove = le[l].eval;
                                        }
                                        l = le[l].next;
                                    }
                                }
                                k = le[k].next;
                            }
                        }
                        j = le[j].next;
                    }
                }
                bmIndex++;
                bestMove = 999;
                i = le[i].next;
            }
        }
        public void whitePly4()
        {
            int i = 1, j = 1, k = 1, l = 1;
            double avg1, avg2, avg3;
            avg1 = calulateAvg(1);
            while (le[i].next != -1)
            {
                if (le[i].eval > avg1)
                {
                    le[i].down = nextFree;
                    getMoves(le[i].bd);
                    avg2 = calulateAvg(i);
                    j = le[i].down;
                    while (le[j].next != -1)
                    {
                        if (le[j].eval > avg2)
                        {
                            le[j].down = nextFree;
                            getMoves(le[j].bd);
                            avg3 = calulateAvg(j);
                            k = le[j].down;
                            while (le[k].next != -1)
                            {
                                if (le[k].eval > avg3)
                                {
                                    le[k].down = nextFree;
                                    getMoves(le[k].bd);
                                    l = le[k].down;
                                    while (le[l].next != -1)
                                    {
                                        if (le[l].eval > bestMove)
                                        {
                                            bm[bmIndex].eval = le[l].eval;
                                            bm[bmIndex].leIndex = l;
                                            bestMove = le[l].eval;
                                        }
                                        l = le[l].next;
                                    }
                                }
                                k = le[k].next;
                            }
                        }
                        j = le[j].next;
                    }
                }
                bmIndex++;
                bestMove = -999;
                i = le[i].next;
            }
        }
        public void blackPly6()
        {
            int i = 1, j = 1, k = 1, l = 1,m=1,n=1;
            double avg1, avg2, avg3,avg4,avg5;
            avg1 = calulateAvg(1);
            while (le[i].next != -1)
            {
                if (le[i].eval < avg1)
                {
                    le[i].down = nextFree;
                    getMoves(le[i].bd);//2
                    avg2 = calulateAvg(i);
                    j = le[i].down;
                    while (le[j].next != -1)
                    {
                        if (le[j].eval < avg2)
                        {
                            le[j].down = nextFree;
                            getMoves(le[j].bd);//3
                            avg3 = calulateAvg(j);
                            k = le[j].down;
                            while (le[k].next != -1)
                            {
                                if (le[k].eval < avg3)
                                {
                                    le[k].down = nextFree;
                                    getMoves(le[k].bd);//4
                                    avg4 = calulateAvg(k);
                                    l = le[k].down;
                                    while (le[l].next != -1)
                                    {
                                        if (le[l].eval < avg4)
                                        {
                                            le[l].down = nextFree;
                                            getMoves(le[l].bd);//5
                                            avg5 = calulateAvg(l);
                                            m = le[l].down;
                                            while (le[m].next != -1)
                                            {
                                                if (le[m].eval < avg5)
                                                {
                                                    le[m].down = nextFree;
                                                    getMoves(le[m].bd);//6
                                                    n = le[m].down;
                                                    while (le[n].next != -1)
                                                    {
                                                        if (le[n].eval < bestMove)
                                                        {
                                                            bm[bmIndex].eval = le[n].eval;
                                                            bm[bmIndex].leIndex = n;
                                                            bestMove = le[n].eval;
                                                        }
                                                        n = le[n].next;
                                                    }
                                                }
                                                m = le[m].next;
                                            }
                                        }
                                        l = le[l].next;
                                    }
                                }
                                k = le[k].next;
                            }
                        }
                        j = le[j].next;
                    }
                }
                bmIndex++;
                bestMove = 999;
                i = le[i].next;
            }
        }
        public void whitePly6()

        {
            int i = 1, j = 1, k = 1, l = 1, m = 1, n = 1;
            double avg1, avg2, avg3, avg4, avg5;
            avg1 = calulateAvg(1);
            while (le[i].next != -1)
            {
                if (le[i].eval > avg1)
                {
                    le[i].down = nextFree;
                    getMoves(le[i].bd);//2
                    avg2 = calulateAvg(i);
                    j = le[i].down;
                    while (le[j].next != -1)
                    {
                        if (le[j].eval > avg2)
                        {
                            le[j].down = nextFree;
                            getMoves(le[j].bd);//3
                            avg3 = calulateAvg(j);
                            k = le[j].down;
                            while (le[k].next != -1)
                            {
                                if (le[k].eval > avg3)
                                {
                                    le[k].down = nextFree;
                                    getMoves(le[k].bd);//4
                                    avg4 = calulateAvg(k);
                                    l = le[k].down;
                                    while (le[l].next != -1)
                                    {
                                        if (le[l].eval > avg4)
                                        {
                                            le[l].down = nextFree;
                                            getMoves(le[l].bd);//5
                                            avg5 = calulateAvg(l);
                                            m = le[l].down;
                                            while (le[m].next != -1)
                                            {
                                                if (le[m].eval > avg5)
                                                {
                                                    le[m].down = nextFree;
                                                    getMoves(le[m].bd);//6
                                                    n = le[m].down;
                                                    while (le[n].next != -1)
                                                    {
                                                        if (le[n].eval > bestMove)
                                                        {
                                                            bm[bmIndex].eval = le[n].eval;
                                                            bm[bmIndex].leIndex = n;
                                                            bestMove = le[n].eval;
                                                        }
                                                        n = le[n].next;
                                                    }
                                                }
                                                m = le[m].next;
                                            }
                                        }
                                        l = le[l].next;
                                    }
                                }
                                k = le[k].next;
                            }
                        }
                        j = le[j].next;
                    }
                }
                bmIndex++;
                bestMove = -999;
                i = le[i].next;
            }
        }
    }
}
